package com.example.pingpongalien.conthea.Adapters.ExpandableAdapter

import android.view.View
import android.widget.TextView
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder
import com.example.pingpongalien.conthea.R

/**
 * Created by AOR on 6/12/17.
 */
class ChildViewHolderCatalogue(view: View) : ChildViewHolder(view) {

    var txt_ubication: TextView
    var txt_expiration: TextView
    var txt_maker: TextView
    var txt_lote: TextView

    init {
        txt_ubication = view.findViewById(R.id.txt_ubication)
        txt_expiration = view.findViewById(R.id.txt_expiration)
        txt_maker = view.findViewById(R.id.txt_maker)
        txt_lote = view.findViewById(R.id.txt_lote)
    }
}