package com.example.pingpongalien.conthea.Activities.Core

import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

/**
 * Created by AOR on 23/11/17.
 */
class CoreModel(presenter: CorePresenter, val sharedPreferences: PreferencesController): Core.Model {

    var presenter: Core.Presenter? = null
    var preferences: PreferencesController? = null

    init {
        this.presenter = presenter
        this.preferences = sharedPreferences
    }

    override fun closeSession( ) {
        preferences!!.saveSession(false)
        preferences!!.closeSession()
        presenter!!.showLoginView()
    }

    override fun getUser() {
        val user = preferences!!.getUser()
        presenter!!.setNameUser(user.name, user.lastName)
    }
}