package com.example.pingpongalien.conthea.Activities.Login.Login.Core

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.TextView
import com.atid.lib.dev.ATRfidReader
import com.example.pingpongalien.conthea.Activities.Core.Core
import com.example.pingpongalien.conthea.Activities.Core.CorePresenter
import com.example.pingpongalien.conthea.Activities.Login.Login.LoginView
import com.example.pingpongalien.conthea.Fragments.DetailTag.DetailTagView
import com.example.pingpongalien.conthea.Fragments.SearchCatalogue.SearchCatalogueView
import com.example.pingpongalien.conthea.Fragments.SearchOrder.SearchOrderView
import com.example.pingpongalien.conthea.Fragments.SetPlace.SetPlaceView
import com.example.pingpongalien.conthea.Fragments.Settings.SettingsView
import com.example.pingpongalien.conthea.Fragments.StartInventory.StartInventoryView
import com.example.pingpongalien.conthea.Fragments.Unsuscribe.UnsuscribeView
import com.example.pingpongalien.conthea.Fragments.VerifyInventory.VerifyInventoryView
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.activity_core.*
import kotlinx.android.synthetic.main.app_bar_menu.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import java.util.*


class CoreView : AppCompatActivity(), Core.View,NavigationView.OnNavigationItemSelectedListener {

    val TAG = "CoreView"

    var presenter: Core.Presenter? = null
    var preferences: PreferencesController? = null
    var controlFragment = 0
    private var mReader: ATRfidReader? = null
    var fragment: Fragment? = null
    var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_core)
        configurateNagiationView()
        setToolbar()
        changeTitleToolbar(getString(R.string.buscar_catalogo))
        centerTitleActionBar()
        setGestureNavigationDrawer()
        preferences = PreferencesController(applicationContext)
        presenter = CorePresenter(this, preferences!!)
        presenter!!.getUser()
        setFirstFragment()
    }

    fun setFirstFragment(){
        this.fragment = SearchCatalogueView()
        controlFragment = 0
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.main_content, this.fragment).commit()
    }

    override fun setNameUser(name: String, lastName: String) {
        val headerLayout = navigation_view.getHeaderView(0)
        val txt_name = headerLayout.findViewById(R.id.txt_name) as TextView
        txt_name.text = "$name $lastName"
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        when(item.itemId){
            R.id.item_search_catalog -> {
                fragment = SearchCatalogueView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.buscar_catalogo))
                controlFragment = 0
            }
            R.id.item_search_order -> {
                fragment = SearchOrderView(true)
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.buscar_orden_salida))
                controlFragment = 1
                removeMenuDetailRead()
            }
            R.id.item_cancel_product -> {
                fragment = UnsuscribeView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.dar_baja))
                controlFragment = 2
            }
            R.id.item_inventory -> {
                fragment = VerifyInventoryView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.verificar_inventario))
                controlFragment = 3
                removeMenuDetailRead()
            }
            R.id.item_set_place ->{
                fragment = SetPlaceView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.asignar_lugar))
                controlFragment = 4
            }
            R.id.item_settings -> {
                fragment = SettingsView.createSettingsView(shouldClose = false, isFirstTime = false)
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.configuracion))
                controlFragment = 5
                removeMenuDetailRead()
            }
            R.id.item_detail_tag -> {
                fragment = DetailTagView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.detalle_etiqueta))
                controlFragment = 6
            }
            R.id.item_start_inventory -> {
                fragment = StartInventoryView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.comenzar_inventario))
                controlFragment = 7
            }
            R.id.item_search_order_in -> {
                fragment = SearchOrderView(false)
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.buscar_orden_entrada))
                controlFragment = 8
                removeMenuDetailRead()
            }
        }
        if(fragment != null){
            val manager = supportFragmentManager
            manager.beginTransaction().replace(R.id.main_content, fragment).commit()
        }else
            showAlertCloseSession()
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        showAlertCloseSession()
    }

    override fun showLoginView() {
        startActivity<LoginView>()
        finish()
    }

    override fun closeSession() {
        presenter!!.closeSession()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        when(controlFragment){
            0 -> (this.fragment as SearchCatalogueView).setKeyDown(keyCode, event!!)
            2 -> (this.fragment as UnsuscribeView).setKeyDown(keyCode, event!!)
            4 -> (this.fragment as SetPlaceView).setKeyDown(keyCode, event!!)
            6 -> (this.fragment as DetailTagView).setKeyDown(keyCode, event!!)
            7 -> (this.fragment as StartInventoryView).setKeyDown(keyCode, event!!)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        when(controlFragment){
            0 -> (this.fragment as SearchCatalogueView).setKeyUp(keyCode, event!!)
            2 -> (this.fragment as UnsuscribeView).setKeyUp(keyCode, event!!)
            4 -> (this.fragment as SetPlaceView).setKeyUp(keyCode, event!!)
            6 -> (this.fragment as DetailTagView).setKeyUp(keyCode, event!!)
            7 -> (this.fragment as StartInventoryView).setKeyUp(keyCode, event!!)
        }
        return super.onKeyUp(keyCode, event)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu
        return true
    }

    fun removeMenuDetailRead(){
        toolbar!!.getMenu().clear()
    }

    fun setToolbar() {
        setSupportActionBar(toolbar)
    }

    fun changeTitleToolbar(title:String){
        supportActionBar!!.setTitle(title)
    }

    fun setGestureNavigationDrawer() {
        val toogle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_abrir, R.string.navigation_drawer_cerrar)
        drawer_layout.addDrawerListener(toogle)
        toogle.syncState()
    }

    fun configurateNagiationView(){
        val transaction: android.support.v4.app.FragmentTransaction? = supportFragmentManager.beginTransaction()
        transaction!!.replace(R.id.main_content, SearchCatalogueView())
        transaction.commit()
        navigation_view.setNavigationItemSelectedListener(this)
    }

    private fun centerTitleActionBar() {
        val textViews: ArrayList<View> = ArrayList()
        window.decorView.findViewsWithText(textViews, title, View.FIND_VIEWS_WITH_TEXT)
        if (textViews.size > 0) {
            var appCompatTextView: AppCompatTextView? = null
            if (textViews.size == 1) {
                appCompatTextView = textViews[0] as AppCompatTextView
            } else {
                for (v in textViews) {
                    if (v.getParent() is Toolbar) {
                        appCompatTextView = v as AppCompatTextView
                        break
                    }
                }
            }
            if (appCompatTextView != null) {
                val params = appCompatTextView.layoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                appCompatTextView.layoutParams = params
                appCompatTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
        }
    }

    fun showAlertCloseSession(){
        alert {
            title = getString(R.string.atencion)
            message = getString(R.string.cerrar_sesion)
            positiveButton(getString(R.string.si)) { closeSession() }
            negativeButton(getString(R.string.no)){}
        }.show()
    }

}
