package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class RequestRemision (
    @SerializedName("productos")
    var productos: List<RequestRemisionProducts>? = null
)
