package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.Gson
import okhttp3.ResponseBody
import java.io.IOException


/**
 * Created by AOR on 22/11/17.
 */
class ResponseError(
        val error:String,
        val message: String) {

    companion object {
        fun fromResponseBody(responseBody: ResponseBody): ResponseError? {
            val gson = Gson()
            try {
                return gson.fromJson<ResponseError>(responseBody.string(), ResponseError::class.java)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return null
        }
    }


}