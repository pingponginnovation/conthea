package com.example.pingpongalien.conthea.Fragments.SearchOrder

import android.content.Context
import com.example.pingpongalien.conthea.Adapters.AdapterOrders
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder

/**
 * Created by AOR on 27/11/17.
 */
class SearchOrderPresenter(viewOrder: SearchOrder.View, context: Context): SearchOrder.Presenter {

    var view: SearchOrder.View? = null
    var model: SearchOrder.Model? = null

    init {
        this.view = viewOrder
        model = SearchOrderModel(this, context)
    }

    override fun getOrders() {
        model!!.getOrders()
    }

    override fun filterOrders(filter: String, adapterOrders: AdapterOrders?, typeSearch: Int) {
        model!!.filterOrders(filter, adapterOrders, typeSearch)
    }

    override fun setTypeSearch(typeSearch: Boolean) {
        model!!.setTypeSearch(typeSearch)
    }

    override fun responseOrderSuccess(responseSuccess: ResponseOrder) {
        view!!.setOrders(responseSuccess)
    }

    override fun responseErrorOrders(error: String) {
        view!!.responseErrorOrders(error)
    }

    override fun showNoOrders() {
        view!!.showNoOrders()
    }

    override fun showOrders() {
        view!!.showOrders()
    }

    override fun setComponentsCorrect(position: Int) {
        model!!.setComponentsCorrect(position)
    }

    override fun visibilityDateAndSearchBar(type: Boolean) {
        view!!.visibilityDateAndSearchBar(type)
    }

    override fun formatDateAndFilterData(year: Int, month: Int, day: Int, adapterOrders: AdapterOrders?) {
        model!!.formatDateAndFilterData(year, month, day, adapterOrders)
    }

    override fun setDate(date: String) {
        view!!.setDate(date)
    }
}