package com.example.pingpongalien.conthea.Models

import com.bignerdranch.expandablerecyclerview.Model.ParentObject

/**
 * Created by AOR on 6/12/17.
 */
class ProductParent(val name : String, var mChildrenList: List<Any>?): ParentObject {

    override fun setChildObjectList(list: List<Any>?) {
        this.mChildrenList = list
    }

    override fun getChildObjectList(): List<Any> {
        return mChildrenList!!
    }
}