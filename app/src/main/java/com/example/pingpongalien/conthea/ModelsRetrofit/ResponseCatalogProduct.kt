package com.example.pingpongalien.conthea.ModelsRetrofit

import java.io.Serializable

data class ResponseCatalogProduct(
	val codigo: String? = null,
	val id: Int? = null,
	val fabricante: String? = null,
	val nombre: String? = null
) : Serializable
