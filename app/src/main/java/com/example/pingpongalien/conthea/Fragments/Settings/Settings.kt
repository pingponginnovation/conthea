package com.example.pingpongalien.conthea.Fragments.Settings

interface Settings {

    interface View{
        fun showSaveUrl()
        fun showErrorUrl()
    }

    interface Presenter{
        //MODEL
        fun saveUrl(url: String)

        //VIEW
        fun showSaveUrl()
        fun showErrorUrl()

    }

    interface Model{
        fun saveUrl(url: String)
    }
}