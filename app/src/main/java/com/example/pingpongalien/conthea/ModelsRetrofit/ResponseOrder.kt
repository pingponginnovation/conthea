package com.example.pingpongalien.conthea.ModelsRetrofit


import com.google.gson.annotations.SerializedName

data class ResponseOrder(

	@field:SerializedName("response")
	val response: List<ResponseOrderItem?>? = null
)