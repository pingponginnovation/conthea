package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

/**
 * Created by AOR on 5/12/17.
 */
class ResponseGetPlaces(
        @field:SerializedName("response")
        val aPlaces: ArrayList<ResponseGetPlacesBody>)