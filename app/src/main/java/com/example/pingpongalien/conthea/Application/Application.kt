package com.example.pingpongalien.conthea.Application

import android.app.Application
import com.example.pingpongalien.conthea.Room.RoomController

/**
 * Created by AOR on 5/12/17.
 */
class Application: Application() {

    override fun onCreate() {
        super.onCreate()
        RoomController.getAppDatabase(applicationContext)
    }
}