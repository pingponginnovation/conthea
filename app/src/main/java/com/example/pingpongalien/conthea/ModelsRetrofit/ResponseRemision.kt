package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

/**
 * Created by AOR on 1/12/17.
 */
class ResponseRemision(
        @field:SerializedName("response")
        val response : String
)