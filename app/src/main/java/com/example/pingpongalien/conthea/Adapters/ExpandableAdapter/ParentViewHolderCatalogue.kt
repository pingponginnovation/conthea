package com.example.pingpongalien.conthea.Adapters.ExpandableAdapter

import android.view.View
import android.widget.TextView
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder
import com.example.pingpongalien.conthea.R

/**
 * Created by AOR on 6/12/17.
 */
class ParentViewHolderCatalogue(view: View): ParentViewHolder(view){
    var txt_title: TextView

    init {
        txt_title = view.findViewById(R.id.txt_title)
    }
}