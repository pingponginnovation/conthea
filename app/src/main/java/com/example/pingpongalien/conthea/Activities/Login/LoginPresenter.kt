package com.example.pingpongalien.conthea.Activities.Login.Login


import android.content.Context
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

/**
 * Created by AOR on 17/11/17.
 */
class LoginPresenter(val viewLogin:Login.View, val sharedPreferences: PreferencesController, val context : Context): Login.Presenter {


    var view: Login.View? = null
    var model: Login.Model? = null

    init {
        this.view = viewLogin
        model = LoginModel(this, sharedPreferences, context)
    }

    override fun makeLogin(user: String, password: String, stateSession: Boolean, preferences: PreferencesController){
        model!!.makeLogin(user, password, stateSession, preferences)
    }

    override fun verifyStateSession(preferences: PreferencesController){
        model!!.verifyStateSession(preferences)
    }

    override fun successRequestLogin() {
        view!!.showCore()
    }

    override fun errorRequestLogin(message: String) {
        view!!.showErrorLogin(message)
    }

}