package com.example.pingpongalien.conthea.Activities.Core

import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

/**
 * Created by AOR on 23/11/17.
 */
class CorePresenter(val viewCore: Core.View, val sharedPreferences: PreferencesController): Core.Presenter {


    var view: Core.View? = null
    var model: Core.Model? = null

    init {
        this.view = viewCore
        model = CoreModel(this, sharedPreferences)
    }

    override fun closeSession() {
        model!!.closeSession()
    }

    override fun showLoginView() {
        view!!.showLoginView()
    }

    override fun setNameUser(name: String, lastName: String) {
        view!!.setNameUser(name, lastName)
    }

    override fun getUser() {
        model!!.getUser()
    }
}