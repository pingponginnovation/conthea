package com.example.pingpongalien.conthea.Activities.Login.Login

import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseError
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLogin
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLoginSuccess
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

/**
 * Created by AOR on 17/11/17.
 */
interface Login {

    interface View{
        fun makeLogin()
        fun showErrorLogin(message: String)
        fun showCore()
        fun verifyStateSession()
    }

    interface Presenter{
        fun makeLogin(user: String, password: String, stateSession: Boolean, preferences: PreferencesController)
        fun successRequestLogin()
        fun errorRequestLogin(message: String)
        fun verifyStateSession(preferences: PreferencesController)
    }

    interface Model{
        fun makeLogin(user: String, password: String, stateSession: Boolean, preferences: PreferencesController)
        fun successRequestLogin(responseSuccess: ResponseLogin, stateSession: Boolean)
        fun errorRequestLogin(responseError: ResponseError)
        fun saveKeepSession(keepSession: Boolean, preferences: PreferencesController)
        fun verifyStateSession(preferences: PreferencesController)
    }

}