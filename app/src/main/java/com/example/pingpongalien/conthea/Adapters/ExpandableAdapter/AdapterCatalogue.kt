package com.example.pingpongalien.conthea.Adapters.ExpandableAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter
import com.example.pingpongalien.conthea.Models.ProductChild
import com.example.pingpongalien.conthea.Models.ProductParent
import com.example.pingpongalien.conthea.R


/**
 * Created by AOR on 6/12/17.
 */
class AdapterCatalogue(context: Context, var aCatalog : List<ProductParent>?) : ExpandableRecyclerAdapter<ParentViewHolderCatalogue, ChildViewHolderCatalogue>(context, aCatalog) {

    var inflater: LayoutInflater? = null

    init {
        this.inflater = LayoutInflater.from(context)
    }

    override fun onBindChildViewHolder(viewHolder: ChildViewHolderCatalogue?, p1: Int, child: Any?) {
        val productChild = child as ProductChild
        viewHolder!!.txt_ubication.text = productChild.ubication
        viewHolder.txt_expiration.text = productChild.expiration
        viewHolder.txt_lote.text = productChild.lote
        viewHolder.txt_maker.text = productChild.maker
    }

    override fun onCreateChildViewHolder(viewGroup: ViewGroup?): ChildViewHolderCatalogue {
        val view = inflater!!.inflate(R.layout.item_catalogue_child, viewGroup, false)
        return ChildViewHolderCatalogue(view)
    }

    override fun onCreateParentViewHolder(viewGroup: ViewGroup?): ParentViewHolderCatalogue {
        val view = inflater!!.inflate(R.layout.item_catalogue_parent, viewGroup, false)
        return ParentViewHolderCatalogue(view)
    }

    override fun onBindParentViewHolder(viewHolder: ParentViewHolderCatalogue?, p1: Int, parent: Any?) {
        val productParent = parent as ProductParent
        viewHolder!!.txt_title.text = productParent.name
    }

}