package com.example.pingpongalien.conthea.Activities.Login.Login

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Fragments.Settings.SettingsView
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginView : AppCompatActivity(), Login.View{

    val TAG = "LoginView"

    var presenter: Login.Presenter? = null
    var preferences: PreferencesController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        preferences = PreferencesController(applicationContext)
        presenter = LoginPresenter(this, preferences!!, applicationContext)
        eventsView()
        verifyStateSession()
        isFirstTimeInApp()
    }

    override fun makeLogin() {
        pb_login.visibility = View.VISIBLE
        presenter!!.makeLogin(edt_user.text.toString(), edt_password.text.toString(), chk_session_started.isChecked, preferences!!)
    }

    override fun showErrorLogin(message:String) {
        pb_login.visibility = View.GONE
        toast(message)
    }

    override fun showCore() {
        startActivity<CoreView>()
        finish()
    }

    override fun verifyStateSession(){
        presenter!!.verifyStateSession(preferences!!)
    }

    fun eventsView(){
        btn_session_start.setOnClickListener { makeLogin() }
        txt_change_url.setOnClickListener { showSettingsUrl(false) }
    }

    fun isFirstTimeInApp(){
        if(preferences!!.getUrl() == "")
            showSettingsUrl(true)
    }

    fun showSettingsUrl(isFirstTime: Boolean) {
        btn_session_start.visibility = View.GONE
        val fragment: Fragment = SettingsView.createSettingsView(true, isFirstTime)
        val manager = supportFragmentManager
        manager.beginTransaction().add(R.id.cl_login, fragment).commit()
    }

}
