package com.example.pingpongalien.conthea.Activities.Login.Login

import android.content.Context
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseError
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLogin
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLoginSuccess
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

/**
 * Created by AOR on 17/11/17.
 */
class LoginModel(val presenterLogin: Login.Presenter, val sharedPreferences: PreferencesController, context: Context): Login.Model {

    var presenter: Login.Presenter? = null
    var preferences: PreferencesController? = null
    var appContext: Context? = null

    init {
        this.presenter = presenterLogin
        this.preferences = sharedPreferences
        this.appContext = context
    }

    override fun makeLogin(user: String, password: String, stateSession: Boolean, preferences: PreferencesController){
        if(validateUser(user) && validatePassword(password)) {
            requestLogin(user, password, stateSession)
        }else
            presenter!!.errorRequestLogin("Agregue un usuario y/o una contraseña mayor a 6 digitos")
    }

    override fun successRequestLogin(responseSuccess: ResponseLogin, stateSession: Boolean){
        saveOnPreferences(responseSuccess, stateSession)
        presenter!!.successRequestLogin()
    }

    override fun errorRequestLogin(responseError: ResponseError){
        presenter!!.errorRequestLogin(responseError.message)
    }

    override fun saveKeepSession(keepSession: Boolean, preferences: PreferencesController) {
        this.preferences!!.saveSession(keepSession)
    }

    override fun verifyStateSession(preferences: PreferencesController){
        if(preferences.getStateSession())
            presenter!!.successRequestLogin()
    }

    fun validateUser(user: String):Boolean{
        if(user.isEmpty())
            return false
        return true
    }

    fun validatePassword(password: String):Boolean{
        if (password.length >= 6)
            return true
        return false
    }

    fun requestLogin(user: String, password: String, stateSession: Boolean){
        val retrofit = RetrofitController(appContext!!)
        retrofit.login(this, user, password, stateSession)
    }

    fun saveOnPreferences(responseSuccess: ResponseLogin, stateSession: Boolean){
        this.preferences!!.saveToken(responseSuccess.token)
        this.preferences!!.saveUser(responseSuccess.user)
        this.preferences!!.saveSession(stateSession)
    }

}