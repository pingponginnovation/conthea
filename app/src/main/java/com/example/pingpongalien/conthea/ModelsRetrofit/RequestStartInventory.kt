package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class RequestStartInventory (
    @SerializedName("uuid")
    var uuids: List<String>? = null
)