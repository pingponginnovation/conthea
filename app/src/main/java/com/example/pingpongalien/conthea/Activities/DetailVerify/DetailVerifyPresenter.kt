package com.example.pingpongalien.conthea.Activities.DetailVerify

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem

/**
 * Created by AOR on 7/12/17.
 */
class DetailVerifyPresenter(view: DetailVerify.View, context: Context) : DetailVerify.Presenter {

    var view: DetailVerify.View? = null
    var model: DetailVerify.Model? = null

    init {
        this.view = view
        this.model = DetailVerifyModel(this, context)
    }

    //MODEL
    override fun addTag(tag: String) {
        model!!.addTagForVerify(tag)
    }

    override fun searchTag() {
        model!!.searchTag()
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model!!.setModeRead(nameItem, item)
    }

    override fun setCatalog(product: ResponseCatalogItem) {
        model!!.setCatalog(product)
    }


    //VIEW
    override fun setItemNameMenu(name: String, item: MenuItem) {
        view!!.setItemNameMenu(name, item)
    }

    override fun setModeRead(mode: Boolean) {
        view!!.setModeRead(mode)
    }

    override fun unsuccessVerify(error: String) {
        view!!.unsuccessVerify(error)
    }

    override fun setAmount(verify: Int, total : Int) {
        view!!.setAmount(verify, total)
    }

    override fun setTotal(total: String) {
        view!!.setTotal(total)
    }

    override fun setColorComplete() {
        view!!.setColorComplete()
    }

    override fun setColorIncomplete() {
        view!!.setColorIncomplete()
    }

}