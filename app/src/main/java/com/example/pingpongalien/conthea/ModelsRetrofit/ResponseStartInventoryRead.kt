package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseStartInventoryRead (
    @SerializedName("leidos")
    var read: String? = null,
    @SerializedName("guardados")
    var saved: String? = null
)