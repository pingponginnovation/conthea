package com.example.pingpongalien.conthea.Fragments.Settings


import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.activity_login.btn_session_start
import kotlinx.android.synthetic.main.fragment_settings.*
import org.jetbrains.anko.longToast


class SettingsView : Fragment(), Settings.View {

    val TAG = "SettingsView"

    var presenter: Settings.Presenter? =null

    var preferences: PreferencesController? = null

    companion object {

        private var shouldClose: Boolean = false
        private var isFirstTime : Boolean = false

        fun createSettingsView(shouldClose : Boolean, isFirstTime : Boolean): Fragment {
            this.shouldClose = shouldClose
            this.isFirstTime = isFirstTime
            return SettingsView()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(com.example.pingpongalien.conthea.R.layout.fragment_settings, container, false)
        preferences = PreferencesController(context)
        presenter = SettingsPresenter(this, preferences!!)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventsView()
        hideLogo()
        setActualUrl()
        setVersionApp()
    }

    override fun showSaveUrl() {
        context.longToast(getString(R.string.success_url))
        if(shouldClose)
            closeFragment()
    }

    override fun showErrorUrl() {
        context.longToast(getString(R.string.error_url))
    }

    private fun setVersionApp() {
        try {
            val pInfo = context.packageManager.getPackageInfo(activity.packageName, 0)
            txt_version_number.text = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun eventsView(){
        btn_save_configuration.setOnClickListener {
            presenter!!.saveUrl(edt_url.text.toString())
        }
        img_close.setOnClickListener {
            closeFragment()
        }

    }

    private fun hideLogo(){
        if(!shouldClose) {
            im_logo.visibility = View.GONE
            img_close.visibility = View.GONE
        }
        if(isFirstTime)
            img_close.visibility = View.GONE
    }

    private fun closeFragment(){
        val manager = activity.supportFragmentManager
        manager.beginTransaction().remove(this).commit()
        activity.btn_session_start.visibility = View.VISIBLE
    }

    private fun setActualUrl(){
        edt_url.setText(preferences!!.getUrl())
    }
}
