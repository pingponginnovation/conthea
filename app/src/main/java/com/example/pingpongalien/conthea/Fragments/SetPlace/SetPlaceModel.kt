package com.example.pingpongalien.conthea.Fragments.SetPlace

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestPlaces
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsePlaces
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

/**
 * Created by AOR on 5/12/17.
 */
class SetPlaceModel(presenter: SetPlacePresenter, context: Context): SetPlace.Model {

    var presenter: SetPlace.Presenter? = null
    var idPlace = 0
    var aPlaces: ResponseGetPlaces? = null
    var aTags = arrayListOf<String>()
    var appContext: Context

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun requestPlaces() {
        val retrofit = RetrofitController(appContext)
        retrofit.getPlaces(this)
    }

    override fun successRequestGetPlaces(response : ResponseGetPlaces){
        if(response.aPlaces.size > 0) {
            this.aPlaces = response
            setPlaces()
            setId()
            setNamePlace()
        } else
            presenter!!.unSuccessRequestPlaces("No existen úbicaciones activas")
    }

    override fun unSuccessRequestGetPlaces(error : String){
        presenter!!.unSuccessRequestPlaces(error)
        aTags.clear()
    }

    override fun successRequestPlaces(response: ResponsePlaces) {
        presenter!!.setTotalUpdate(response.elementos_actualizados.toString())
        aTags.clear()
    }

    override fun setidPlace(position: Int) {
        idPlace = aPlaces!!.aPlaces[position].id!!
    }

    override fun getName(position : Int) {
        presenter!!.setNamePlace(aPlaces!!.aPlaces[position].almacen!!.nombre!!)
    }

    override fun addTag(tag: String) {
        aTags.add(tag)
    }

    override fun sendTags() {
        val retrofit = RetrofitController(appContext)
        retrofit.sendTags(RequestPlaces(idPlace, aTags), this)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura continua")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura única", item)
        } else{
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura continua", item)
        }
    }

    fun setPlaces(){
        val aNames = ArrayList<String>()
        aPlaces!!.aPlaces.forEach{ place ->
            aNames.add(place.codigo!!)
        }
        presenter!!.setPlaces(aNames)
    }

    fun setNamePlace(){
        presenter!!.setNamePlace(aPlaces!!.aPlaces[0].almacen!!.nombre!!)
    }

    fun setId(){
        idPlace = aPlaces!!.aPlaces[0].id!!
    }
}