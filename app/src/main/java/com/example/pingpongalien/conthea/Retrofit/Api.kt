package com.example.pingpongalien.conthea.Retrofit

import com.example.pingpongalien.conthea.ModelsRetrofit.*
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by AOR on 17/11/17.
 */
interface Api {

    @POST("login")
    fun login(@Body requestLogin: RequestLogin) : Call<ResponseLogin>

    @POST("lote/verificacion")
    fun verifyTagsOut(@Header("Authorization")token : String, @Body requestVerification : RequestVerifyTag) : Call<ResponseVerifyTag>

    @POST("lote/verificacion/reingreso")
    fun verifyTagsIn(@Header("Authorization")token : String, @Body requestVerification : RequestVerifyTag) : Call<ResponseVerifyTag>

    @POST("almacen/salida/productos")
    fun sendRemisionOut(@Header("Authorization")token : String, @Body requestOrders: RequestRemision) : Call<ResponseRemision>

    @POST("almacen/entrada/productos")
    fun sendRemisionIn(@Header("Authorization")token : String, @Body requestOrders: RequestRemision) : Call<ResponseRemision>

    @POST("producto/ubicacion")
    fun sendTags(@Header("Authorization")token : String, @Body requestPlaces : RequestPlaces) : Call<ResponsePlaces>

    @POST("producto/ubicacion/verificar")
    fun verifyUbication(@Header("Authorization")token : String, @Body requestUbication : RequestVerify) : Call<ResponseVerify>

    @POST("items/borrar")
    fun deleteProduct(@Header("Authorization")token : String, @Body requestDelete: RequestDelete) : Call<ResponseDelete>

    @POST("items/info")
    fun detailTag(@Header("Authorization")token : String, @Body requestDetailTag : RequestDetailTag) : Call<ResponseDetailTag>

    @POST("productos/nombres")
    fun getNameUuid(@Header("Authorization")token : String, @Body requestNameUuid: RequestNameUuid) : Call<ResponseNameUuid>

    @POST("inventario-inicial/sobrante")
    fun startInventory(@Header("Authorization") token: String, @Body requestStartInventory: RequestStartInventory) : Call<ResponseStartInventory>

    @GET("producto/ubicacion/{idProducto}")
    fun getPlaces(@Header("Authorization")token : String, @Path ("idProducto") idProducto : Int) : Call<ResponseCatalog>

    @GET("almacen/salida/pendiente")
    fun getOrdersOut(@Header("Authorization")token : String) : Call<ResponseOrder>

    @GET("almacen/entrada/pendiente")
    fun getOrdersIn(@Header("Authorization")token : String) : Call<ResponseOrder>

    @GET("ubicaciones")
    fun getPlaces(@Header("Authorization")token : String) : Call<ResponseGetPlaces>

    @GET("productos")
    fun getProducts(@Header("Authorization")token : String) : Call<List<ResponseProducts>>

    @GET("producto/ubicacion")
    fun getCataloge(@Header("Authorization")token : String) : Call<ResponseCatalog>

}