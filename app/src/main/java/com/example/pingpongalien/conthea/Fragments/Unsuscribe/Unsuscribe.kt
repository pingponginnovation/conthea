package com.example.pingpongalien.conthea.Fragments.Unsuscribe

import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces

interface Unsuscribe {

    interface View{
        fun setTotalUpdate(totalUpdate : String)
        fun showErrorGetPlaces(error : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
    }

    interface Presenter{
        //MODEL
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)

        //VIEW
        fun setTotalUpdate(totalUpdate : String)
        fun unSuccessRequestPlaces(error : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)

    }

    interface Model{
        fun successRequestPlaces(response : ResponseDelete)
        fun unSuccessRequestGetPlaces(error : String)
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)
    }
}