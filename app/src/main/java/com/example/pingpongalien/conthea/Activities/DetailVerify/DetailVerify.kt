package com.example.pingpongalien.conthea.Activities.DetailVerify

import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseVerify

/**
 * Created by AOR on 7/12/17.
 */
interface DetailVerify {

    interface View{
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun unsuccessVerify(error : String)
        fun setAmount(verify: Int, total : Int)
        fun setTotal(total : String)
        fun setColorComplete()
        fun setColorIncomplete()
    }

    interface Presenter {
        //MODEL
        fun addTag(tag: String)
        fun searchTag()
        fun setModeRead(nameItem : String, item: MenuItem)
        fun setCatalog(product : ResponseCatalogItem)


        //VIEW
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun unsuccessVerify(error : String)
        fun setAmount(verify: Int, total : Int)
        fun setTotal(total : String)
        fun setColorComplete()
        fun setColorIncomplete()

    }

    interface Model{
        fun addTagForVerify(tag: String)
        fun searchTag()
        fun setModeRead(nameItem : String, item: MenuItem)
        fun successVerify(response : ResponseVerify)
        fun unsuccessVerify(error : String)
        fun setCatalog(product : ResponseCatalogItem)
    }

}