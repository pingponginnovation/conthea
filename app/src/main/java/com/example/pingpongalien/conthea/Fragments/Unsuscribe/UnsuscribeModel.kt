package com.example.pingpongalien.conthea.Fragments.Unsuscribe

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

class UnsuscribeModel(presenter : Unsuscribe.Presenter, context: Context): Unsuscribe.Model {

    var presenter: Unsuscribe.Presenter? = null

    val TAG = "UnsuscribeModel"
    var aTags = arrayListOf<String>()
    var appContext: Context

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun successRequestPlaces(response: ResponseDelete) {
        presenter!!.setTotalUpdate(response.response.toString())
        aTags.clear()
    }

    override fun unSuccessRequestGetPlaces(error : String){
        presenter!!.unSuccessRequestPlaces(error)
        aTags.clear()
    }

    override fun addTag(tag: String) {
        aTags.add(tag)
    }

    override fun sendTags() {
        val retrofit = RetrofitController(appContext)
        retrofit.deleteTags(RequestDelete(aTags), this)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura continua")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura única", item)
        } else{
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura continua", item)
        }
    }
}