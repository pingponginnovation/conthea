package com.example.pingpongalien.conthea.ModelsRetrofit

data class ResponseDetailTagDetail(
        val id: Int,
        val catalogo : String,
        val descripcion : String,
        val lote : String,
        val caducidad : String,
        val costo : Double,
        var moneda : String,
        val ubicacion : String
)