package com.example.pingpongalien.conthea.Fragments.SetPlace


import android.app.AlertDialog
import android.os.Bundle
import android.os.SystemClock
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.atid.lib.dev.ATRfidReader
import com.atid.lib.dev.rfid.ATRfid900MAReader
import com.atid.lib.dev.rfid.exception.ATRfidReaderException
import com.atid.lib.dev.rfid.param.RangeValue
import com.atid.lib.dev.rfid.type.ActionState
import com.atid.lib.dev.rfid.type.ConnectionState
import com.atid.lib.dev.rfid.type.ResultCode
import com.atid.lib.dev.rfid.type.TagType
import com.atid.lib.diagnostics.ATLog
import com.atid.lib.system.device.type.RfidModuleType
import com.atid.lib.util.SysUtil
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.Rfid.CommonDialog
import com.example.pingpongalien.conthea.Rfid.GlobalInfo
import com.example.pingpongalien.conthea.Rfid.ReaderFragment
import com.example.pingpongalien.conthea.Rfid.SoundPlay
import kotlinx.android.synthetic.main.fragment_set_place.*
import org.jetbrains.anko.toast


class SetPlaceView : ReaderFragment(), SetPlace.View {

    val TAG = "SetPlaceView"


    var presenter: SetPlace.Presenter? = null

    private var mPowerRange: RangeValue? = null
    private var mOperationTime = 0
    private var mPowerLevel: Int = 0
    private var mIsReportRssi = false
    private var mTagType: TagType? = null
    private var mElapsedTick: Long = 0
    private var mTick: Long = 0
    private val SKIP_KEY_EVENT_TIME: Long = 1000
    private var mThread: Thread? = null
    private var mIsAliveThread = false
    private var mSound: SoundPlay? = null
    private var mMAReader: ATRfid900MAReader? = null
    private val UPDATE_TIME = 500
    private var m_timeFlag = 0
    private var m_timeSec: Long = 1
    private val MAX_POWER_LEVEL = 300
    //TRUE CONTINUE FALSE SINGLE
    private var modeRead = false
    private val POWER_LEVEL = 200

    var builder: AlertDialog.Builder? = null
    var txt_total: TextView? = null
    var viewDialog: View? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_set_place, container, false)
        presenter = SetPlacePresenter(this, context)
        presenter!!.getPlaces()
        createDialogTotal()
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSound = SoundPlay(context)
        mPowerRange = null
        mPowerLevel = MAX_POWER_LEVEL
        mOperationTime = 0
        mTagType = TagType.Tag6C
        mTick = 0
        mElapsedTick = 0
        eventView()
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_detail_verify, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setPlaces(aPlaces : ArrayList<String>) {
        sp_places.adapter = ArrayAdapter<String>(context, R.layout.item_spinner, aPlaces)
    }

    override fun showErrorGetPlaces(error: String) {
        context.toast(error)
    }

    override fun setNamePlace(name: String) {
        txt_name.text = name
    }

    override fun setTotalUpdate(totalUpdate: String) {
        showTotal(totalUpdate)
    }

    override fun setModeRead(mode: Boolean){
        GlobalInfo.setContinuousMode(mode)
        this.modeRead = mode
    }

    fun eventView(){
        sp_places.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                presenter!!.setidPlace(position)
                presenter!!.getName(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        })
    }

    fun createDialogTotal(){
        this.builder = AlertDialog.Builder(context)
        val layoutInflater = LayoutInflater.from(context)
        this.viewDialog = layoutInflater.inflate(R.layout.dialog_set_place_unsuscribe, null)
        this.txt_total = viewDialog!!.findViewById(R.id.txt_total)
// You Can Customise your Title here
        this.builder!!.setView(viewDialog)
                .setPositiveButton(getString(R.string.aceptar),{dialog, which ->
                }  )

    }

    fun showTotal(totalUpdate: String){
        this.txt_total!!.text = totalUpdate
        this.builder!!.show()
        controlateViewTotal()
    }

    fun controlateViewTotal(){
        if(this.txt_total!!.parent != null)
            (this.viewDialog!!.parent as ViewGroup).removeView(txt_total)
        this.viewDialog = layoutInflater.inflate(R.layout.dialog_set_place_unsuscribe, null)
        this.txt_total = viewDialog!!.findViewById(R.id.txt_total)
        this.builder!!.setView(viewDialog)
    }

    private fun showPotency() {
        CommonDialog.showPowerGainDialog(context, R.string.power_gain, getPowerLevel(), mPowerRange,
                mPowerGainListener)
    }

    protected fun getPowerLevel(): Int {
        return mPowerLevel
    }

    private val mPowerGainListener = CommonDialog.IPowerGainDialogListener { value, _ ->
        try {
            mReader.power = value
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e,
                    "ERROR. mPowerGainListener.\$CommonDialog.IPowerGainDialogListener.onSelected(%d) - Failed to set power gain",
                    value)
            return@IPowerGainDialogListener
        }

        setPowerLevel(value)
        ATLog.i(TAG, "INFO. mPowerGainListener.\$CommonDialog.IPowerGainDialogListener.onSelected(%d)", value)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        item.title = name
    }

    override fun initReader() {
        try {
            mPowerRange = mReader!!.powerRange
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get power range")
        }

        ATLog.i(TAG, "INFO. initReader() - [Power Range : %d, %d]", mPowerRange!!.getMin(), mPowerRange!!.getMax())

        // Get Power Level
        try {
            mPowerLevel = POWER_LEVEL
            mReader.power = POWER_LEVEL
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get power level")
        }

        ATLog.i(TAG, "INFO. initReader() - [Power Level : %d]", mPowerLevel)

        // Get Operation Time
        try {
            mOperationTime = mReader!!.operationTime
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get operation time")
        }

        //ATLog.i(TAG, "INFO. initReader() - [Operation Time : %d]", mOperationTime);
        // Get Report RSSI
        try {
            mIsReportRssi = mReader!!.reportRssi
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get report RSSI")
        }

        ATLog.i(TAG, "INFO. initReader() - [Report RSSI : %s]", mIsReportRssi)

        ATLog.i(TAG, "INFO initReader()")
    }

    override fun activateReader() {
        // Set Power Level
        setPowerLevel(mPowerLevel)

        // Set Operation Time
        setOperationTime(mOperationTime)

        // Set Tag Type
        setTagType(mTagType!!)

        ATLog.i(TAG, "INFO. activateReader()")
    }

    protected fun setPowerLevel(power: Int) {
        //POTENCIA
        mPowerLevel = power
    }

    protected fun setOperationTime(time: Int) {
        mOperationTime = time
    }

    protected fun setTagType(type: TagType) {
        mTagType = type
    }

    fun setKeyDown(keyCode : Int, event: KeyEvent){
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT
                || keyCode == KeyEvent.KEYCODE_F7 || keyCode == KeyEvent.KEYCODE_F8) && event.repeatCount <= 0
                && mReader!!.action == ActionState.Stop && mReader!!.state == ConnectionState.Connected) {

            ATLog.i(TAG, "INFO. onKeyDown(%d, %d)", keyCode, event.action)

            mElapsedTick = SystemClock.elapsedRealtime() - mTick
            if (mTick == 0L || mElapsedTick > SKIP_KEY_EVENT_TIME) {
                startAction()
                mTick = SystemClock.elapsedRealtime()
            } else {
                ATLog.e(TAG, "INFO. Skip key down event(elapsed:$mElapsedTick)")
            }
        }

    }

    fun setKeyUp(keyCode : Int, event: KeyEvent){
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT
                || keyCode == KeyEvent.KEYCODE_F7 || keyCode == KeyEvent.KEYCODE_F8) && event.repeatCount <= 0
                && mReader!!.action != ActionState.Stop && mReader!!.state == ConnectionState.Connected) {

            ATLog.i(TAG, "INFO. onKeyUp(%d, %d)", keyCode, event.action)

            stopAction()
        }
    }


    override fun onReaderActionChanged(reader: ATRfidReader, action: ActionState) {
        if (action == ActionState.Stop) {
            stopUpdateTagCount()
        }
    }

    override fun onReaderReadTag(reader: ATRfidReader, tag: String, rssi: Float, phase: Float) {
        presenter!!.addTag(tag.substring(4))
        playSuccess()
        if(!modeRead)
            presenter!!.sendTags()
    }

    protected fun playSuccess() {
        mSound!!.playSuccess()
    }

    private val mTagTypeListener = CommonDialog.ITagTypeListener { value, _ -> setTagType(value) }

    protected fun startAction() {
        var res: ResultCode
        val tagType = this.getTagType()

        if (mReader!!.moduleType == RfidModuleType.I900MA) {
            mMAReader = mReader as ATRfid900MAReader
            if (modeRead) {

                // Multi Reading
                startUpdateTagCount()
                //SysUtil.sleep(1000);

                //if ((res = mReader.inventory6cTag()) != ResultCode.NoError) {
                if (tagType == TagType.Tag6B) {
                    res = mMAReader!!.inventory6bTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory 6B tag [%s]",
                                res)
                        stopUpdateTagCount()
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.Tag6C) {
                    res = mMAReader!!.inventory6cTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory 6C tag [%s]",
                                res)
                        stopUpdateTagCount()
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagRail) {
                    res = mMAReader!!.inventoryRailTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory Rail tag [%s]",
                                res)
                        stopUpdateTagCount()
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagAny) {
                    res = mMAReader!!.inventoryAnyTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory Any tag [%s]",
                                res)
                        stopUpdateTagCount()
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                }
            } else {
                // Single Reading
                if (tagType == TagType.Tag6B) {
                    res = mMAReader!!.readEpc6bTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read 6B tag [%s]", res)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.Tag6C) {
                    res = mMAReader!!.readEpc6cTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read 6C tag [%s]", res)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagRail) {
                    res = mMAReader!!.readEpcRailTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read Rail tag [%s]", res)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagAny) {
                    res = mMAReader!!.readEpcAnyTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read Any tag [%s]", res)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(context, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                }
            }
        } else {
            if (modeRead) {

                if (mReader!!.action != ActionState.Stop) {
                    ATLog.e(TAG, "ActionState is not idle.")
                    return
                }

                // Multi Reading
                startUpdateTagCount()
                res = mReader!!.inventory6cTag()
                if (res != ResultCode.NoError) {
                    ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory 6C tag [%s]",
                            res)
                    stopUpdateTagCount()
                    return
                }

            } else {
                // Single Reading
                res = mReader!!.readEpc6cTag()
                if (res != ResultCode.NoError) {
                    ATLog.e(TAG,
                            "ERROR. startAction() - Failed to start read 6C tag [%s]", res)

                    return
                }
            }
        }

        ATLog.i(TAG, "INFO. startAction()")
    }

    protected fun stopAction() {

        if (mReader!!.action == ActionState.Stop) {
            ATLog.e(TAG, "ActionState is not busy.")
            return
        }

        val res: ResultCode
        res = mReader!!.stop()
        if (res != ResultCode.NoError) {
            ATLog.e(TAG, "ERROR. stopAction() - Failed to stop operation [%s]", res)
            return
        }

        presenter!!.sendTags()
        ATLog.i(TAG, "INFO. stopAction()")
    }

    private fun stopUpdateTagCount() {
        if (mThread == null)
            return

        mIsAliveThread = false
        try {
            mThread!!.join()
        } catch (e: InterruptedException) {
            ATLog.e(TAG, "ERROR. stopUpdateTagCount() - Failed to join update list thread", e)
        }

        mThread = null

        ATLog.i(TAG, "INFO. stopUpdateTagCount()")
    }

    protected fun getTagType(): TagType {
        return mTagType!!
    }

    private fun startUpdateTagCount() {
        mThread = Thread(mTimerThread)
        mThread!!.start()

        while (!mIsAliveThread) {
            SysUtil.sleep(5)
        }

        ATLog.i(TAG, "INFO. startUpdateTagCount()")
    }

    private val mTimerThread = Runnable {
        mIsAliveThread = true

        while (mIsAliveThread) {
            activity.runOnUiThread(mUpdateList)
            SysUtil.sleep(UPDATE_TIME.toLong())
        }
    }

    private val mUpdateList = Runnable {
        //			synchronized (adpTags) {
        //txtCount.setText(String.format(Locale.US, "%d", adpTags.getCount()));
        //txtTotalCount.setText(String.format(Locale.US, "%d", m_totalCount));

        m_timeFlag = m_timeFlag xor 1
        if (0 == m_timeFlag) {
            //m_tagTpsCount = m_totalCount / m_timeSec
            m_timeSec++
        }

    }


}
