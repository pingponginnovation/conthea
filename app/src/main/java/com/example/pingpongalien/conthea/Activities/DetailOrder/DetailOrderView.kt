package com.example.pingpongalien.conthea.Activities.DetailOrder

import android.app.AlertDialog
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ListView
import android.widget.SimpleAdapter
import android.widget.TextView
import android.widget.Toast
import com.atid.lib.dev.ATRfidReader
import com.atid.lib.dev.rfid.ATRfid900MAReader
import com.atid.lib.dev.rfid.exception.ATRfidReaderException
import com.atid.lib.dev.rfid.param.RangeValue
import com.atid.lib.dev.rfid.type.ActionState
import com.atid.lib.dev.rfid.type.ConnectionState
import com.atid.lib.dev.rfid.type.ResultCode
import com.atid.lib.dev.rfid.type.TagType
import com.atid.lib.diagnostics.ATLog
import com.atid.lib.system.device.type.RfidModuleType
import com.atid.lib.util.SysUtil
import com.example.pingpongalien.conthea.Adapters.AdapterNoMatch
import com.example.pingpongalien.conthea.Adapters.AdapterProducts
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderProduct
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.Rfid.CommonDialog
import com.example.pingpongalien.conthea.Rfid.GlobalInfo
import com.example.pingpongalien.conthea.Rfid.ReaderActivity
import com.example.pingpongalien.conthea.Rfid.SoundPlay
import kotlinx.android.synthetic.main.activity_detail_order.*
import org.jetbrains.anko.*


class DetailOrderView : ReaderActivity(), DetailOrder.View{

    val TAG = "DetailOrderView"

    var presenter: DetailOrder.Presenter? = null

    var responseOrderItem: ResponseOrderItem? = null

    var adapter: AdapterProducts? = null

    private var mPowerRange: RangeValue? = null
    private var mOperationTime = 0
    private var mPowerLevel: Int = 0
    private var mIsReportRssi = false
    private var mTagType: TagType? = null
    private var mElapsedTick: Long = 0
    private var mTick: Long = 0
    private val SKIP_KEY_EVENT_TIME: Long = 1000
    private var mThread: Thread? = null
    private var mIsAliveThread = false
    private var mSound: SoundPlay? = null
    private var mMAReader: ATRfid900MAReader? = null
    private val UPDATE_TIME = 500
    private var m_timeFlag = 0
    private var m_timeSec: Long = 1
    private val MAX_POWER_LEVEL = 300

    private var modeRead = true
    private val POWER_LEVEL = 200


    init{
        mView = R.layout.activity_detail_order
        mPowerRange = null
        mPowerLevel = MAX_POWER_LEVEL
        mOperationTime = 0
        mTagType = TagType.Tag6C
        mTick = 0
        mElapsedTick = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_order)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        mSound = SoundPlay(this)
        setDataView()
        setAdapter()
        presenter = DetailOrderPresenter(this, applicationContext)
        setLotes()
        setProducts()
        setAdapterModel()
        setSupportActionBar(tb_detail)
        setTotal(0)
        presenter!!.setTypeSearch(intent.getBooleanExtra("type", false))
    }

    override fun successRequestRemision(response: String) {
        alert {
            title = getString(R.string.exito)
            message = response
            positiveButton(getString(R.string.aceptar)){ finish()}
        }.show()
    }

    override fun unsuccessRequestRemision(error: String) {
        longToast(error)
    }

    override fun tagsNoMatch(aTagsNoMatch:  ArrayList<TagNoMatch>) {
        showDialogNoMatch(aTagsNoMatch)
    }

    override fun setModeRead(mode: Boolean){
        GlobalInfo.setContinuousMode(mode)
        this.modeRead = mode
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        item.title = name
    }

    private fun showDialogNoMatch(aTagsNoMatch: ArrayList<TagNoMatch>){
        val builder = AlertDialog.Builder(this)
        val layoutInflater = LayoutInflater.from(this)
        val adapter = AdapterNoMatch(aTagsNoMatch)
        val view = layoutInflater.inflate(R.layout.dialog_no_match, null)
        val rcv_tags_no_match = view.findViewById(R.id.rcv_tags_no_match) as RecyclerView
        rcv_tags_no_match.layoutManager = LinearLayoutManager(applicationContext)
        rcv_tags_no_match.adapter = adapter
// You Can Customise your Title here
        builder.setView(view)
                .setCustomTitle(createTitleCustomised())
                .setPositiveButton(getString(R.string.aceptar),{dialog, which ->  }  )
                .show()
    }

    private fun createTitleCustomised():TextView{
        val txt_title = TextView(this)
        txt_title.text = "Productos que no pertenecen al pedido"
        txt_title.setPadding(10,10,10,10)
        txt_title.gravity = Gravity.CENTER
        txt_title.textSize = 18f
        return txt_title
    }

    private fun setDataView(){
        responseOrderItem = intent.getSerializableExtra("order") as ResponseOrderItem
        txt_name_order.text = getString(R.string.pedido_nombre, responseOrderItem!!.codigo)
    }

    private fun setAdapter(){
        rcv_products.layoutManager = LinearLayoutManager(applicationContext)
        adapter = AdapterProducts(responseOrderItem!!.productos as ArrayList<ResponseOrderProduct>)
        rcv_products.adapter = adapter
    }

    private fun setLotes(){
        presenter!!.setLotes(responseOrderItem!!.productos as List<ResponseOrderProduct>)
    }

    private fun setProducts(){
        presenter!!.setProducts(responseOrderItem!!.productos as List<ResponseOrderProduct>)
    }

    private fun setAdapterModel(){
        presenter!!.setAdapter(adapter!!)
    }

    override fun setTotal(total : Int){
        txt_total.text = "$total / ${responseOrderItem!!.productos!!.size}"
    }

    override fun showNoProductsToSend() {
        if(intent.getBooleanExtra("type", false)) {
            longToast(getString(R.string.no_productos_salida))
        } else {
            longToast(getString(R.string.no_productos_entrada))
        }
    }

    override fun showNoCompleteOrder() {
        alert(getString(R.string.orden_no_lista), getString(R.string.atencion)){
            yesButton { presenter!!.sendData() }
            noButton {  }
        }.show()
    }

    private fun showPotency() {
        CommonDialog.showPowerGainDialog(this, R.string.power_gain, getPowerLevel(), mPowerRange,
                mPowerGainListener)
    }

    protected fun getPowerLevel(): Int {
        return mPowerLevel
    }

    private val mPowerGainListener = CommonDialog.IPowerGainDialogListener { value, _ ->
        try {
            mReader.power = value
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e,
                    "ERROR. mPowerGainListener.\$CommonDialog.IPowerGainDialogListener.onSelected(%d) - Failed to set power gain",
                    value)
            return@IPowerGainDialogListener
        }

        setPowerLevel(value)
        ATLog.i(TAG, "INFO. mPowerGainListener.\$CommonDialog.IPowerGainDialogListener.onSelected(%d)", value)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail_order, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_send -> presenter!!.sendData(responseOrderItem!!.productos!!.size)
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initWidgets() {
    }

    override fun enableWidgets(enabled: Boolean) {

    }

    override fun initReader() {
        try {
            mPowerRange = mReader!!.powerRange
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get power range")
        }

        ATLog.i(TAG, "INFO. initReader() - [Power Range : %d, %d]", mPowerRange!!.getMin(), mPowerRange!!.getMax())

        // Get Power Level
        try {
            mPowerLevel = POWER_LEVEL
            mReader.power = POWER_LEVEL
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get power level")
        }

        ATLog.i(TAG, "INFO. initReader() - [Power Level : %d]", mPowerLevel)

        // Get Operation Time
        try {
            mOperationTime = mReader!!.operationTime
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get operation time")
        }

        //ATLog.i(TAG, "INFO. initReader() - [Operation Time : %d]", mOperationTime);
        // Get Report RSSI
        try {
            mIsReportRssi = mReader!!.reportRssi
        } catch (e: ATRfidReaderException) {
            ATLog.e(TAG, e, "ERROR. initReader() - Failed to get report RSSI")
        }

        ATLog.i(TAG, "INFO. initReader() - [Report RSSI : %s]", mIsReportRssi)

        ATLog.i(TAG, "INFO initReader()")
    }

    override fun activateReader() {
        // Set Power Level
        setPowerLevel(mPowerLevel)

        // Set Operation Time
        setOperationTime(mOperationTime)

        // Set Tag Type
        setTagType(mTagType!!)

        ATLog.i(TAG, "INFO. activateReader()")
    }

    protected fun setPowerLevel(power: Int) {
        //POTENCIA
        mPowerLevel = power
    }

    protected fun setOperationTime(time: Int) {
        mOperationTime = time
    }

    protected fun setTagType(type: TagType) {
        mTagType = type
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        //
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT
                || keyCode == KeyEvent.KEYCODE_F7 || keyCode == KeyEvent.KEYCODE_F8) && event.repeatCount <= 0
                && mReader!!.action == ActionState.Stop && mReader!!.state == ConnectionState.Connected) {

            ATLog.i(TAG, "INFO. onKeyDown(%d, %d)", keyCode, event.action)

            mElapsedTick = SystemClock.elapsedRealtime() - mTick
            if (mTick == 0L || mElapsedTick > SKIP_KEY_EVENT_TIME) {
                startAction()
                mTick = SystemClock.elapsedRealtime()
            } else {
                ATLog.e(TAG, "INFO. Skip key down event(elapsed:$mElapsedTick)")
                return super.onKeyDown(keyCode, event)
            }

            return true
        }

        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {

        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT
                || keyCode == KeyEvent.KEYCODE_F7 || keyCode == KeyEvent.KEYCODE_F8) && event.repeatCount <= 0
                && mReader!!.action != ActionState.Stop && mReader!!.state == ConnectionState.Connected) {

            ATLog.i(TAG, "INFO. onKeyUp(%d, %d)", keyCode, event.action)

            stopAction()

            return true
        }

        return super.onKeyUp(keyCode, event)
    }

    override fun onReaderActionChanged(reader: ATRfidReader, action: ActionState) {
        if (action == ActionState.Stop) {
            stopUpdateTagCount()
        }
    }

    override fun onReaderReadTag(reader: ATRfidReader, tag: String, rssi: Float, phase: Float) {
        presenter!!.addTag(tag.substring(4))
        playSuccess()
        if(!modeRead)
            presenter!!.searchTag()
    }

    protected fun playSuccess() {
        mSound!!.playSuccess()
    }

    private val mTagTypeListener = CommonDialog.ITagTypeListener { value, _ -> setTagType(value) }

    protected fun startAction() {
        var res: ResultCode
        val tagType = this.getTagType()

        enableWidgets(false)

        if (mReader!!.moduleType == RfidModuleType.I900MA) {
            mMAReader = mReader as ATRfid900MAReader
            if (modeRead) {

                // Multi Reading
                startUpdateTagCount()
                //SysUtil.sleep(1000);

                //if ((res = mReader.inventory6cTag()) != ResultCode.NoError) {
                if (tagType == TagType.Tag6B) {
                    res = mMAReader!!.inventory6bTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory 6B tag [%s]",
                                res)
                        stopUpdateTagCount()
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.Tag6C) {
                    res = mMAReader!!.inventory6cTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory 6C tag [%s]",
                                res)
                        stopUpdateTagCount()
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagRail) {
                    res = mMAReader!!.inventoryRailTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory Rail tag [%s]",
                                res)
                        stopUpdateTagCount()
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagAny) {
                    res = mMAReader!!.inventoryAnyTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory Any tag [%s]",
                                res)
                        stopUpdateTagCount()
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                }
            } else {
                // Single Reading
                if (tagType == TagType.Tag6B) {
                    res = mMAReader!!.readEpc6bTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read 6B tag [%s]", res)
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.Tag6C) {
                    res = mMAReader!!.readEpc6cTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read 6C tag [%s]", res)
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagRail) {
                    res = mMAReader!!.readEpcRailTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read Rail tag [%s]", res)
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                } else if (tagType == TagType.TagAny) {
                    res = mMAReader!!.readEpcAnyTag()
                    if (res != ResultCode.NoError) {
                        ATLog.e(TAG,
                                "ERROR. startAction() - Failed to start read Any tag [%s]", res)
                        enableWidgets(true)
                        if (res == ResultCode.NotSupported)
                            Toast.makeText(this, R.string.not_supported, Toast.LENGTH_SHORT).show()
                        return
                    }
                }
            }
        } else {
            if (modeRead) {

                if (mReader!!.action != ActionState.Stop) {
                    ATLog.e(TAG, "ActionState is not idle.")
                    return
                }

                // Multi Reading
                startUpdateTagCount()
                res = mReader!!.inventory6cTag()
                if (res != ResultCode.NoError) {
                    ATLog.e(TAG, "ERROR. startAction() - Failed to start inventory 6C tag [%s]",
                            res)
                    stopUpdateTagCount()
                    enableWidgets(true)
                    return
                }

            } else {
                // Single Reading
                res = mReader!!.readEpc6cTag()
                if (res != ResultCode.NoError) {
                    ATLog.e(TAG,
                            "ERROR. startAction() - Failed to start read 6C tag [%s]", res)

                    enableWidgets(true)
                    return
                }
            }
        }

        ATLog.i(TAG, "INFO. startAction()")
    }

    protected fun stopAction() {

        if (mReader!!.action == ActionState.Stop) {
            ATLog.e(TAG, "ActionState is not busy.")
            return
        }
        val res: ResultCode

        enableWidgets(false)
        res = mReader!!.stop()
        if (res != ResultCode.NoError) {
            ATLog.e(TAG, "ERROR. stopAction() - Failed to stop operation [%s]", res)
            enableWidgets(true)
            return
        }
        presenter!!.searchTag()
        ATLog.i(TAG, "INFO. stopAction()")
    }

    private fun stopUpdateTagCount() {
        if (mThread == null)
            return

        mIsAliveThread = false
        try {
            mThread!!.join()
        } catch (e: InterruptedException) {
            ATLog.e(TAG, "ERROR. stopUpdateTagCount() - Failed to join update list thread", e)
        }

        mThread = null

        ATLog.i(TAG, "INFO. stopUpdateTagCount()")
    }

    protected fun getTagType(): TagType {
        return mTagType!!
    }

    private fun startUpdateTagCount() {
        mThread = Thread(mTimerThread)
        mThread!!.start()

        while (!mIsAliveThread) {
            SysUtil.sleep(5)
        }

        ATLog.i(TAG, "INFO. startUpdateTagCount()")
    }

    private val mTimerThread = Runnable {
        mIsAliveThread = true

        while (mIsAliveThread) {
            runOnUiThread(mUpdateList)
            SysUtil.sleep(UPDATE_TIME.toLong())
        }
    }

    private val mUpdateList = Runnable {
        //			synchronized (adpTags) {
        //txtCount.setText(String.format(Locale.US, "%d", adpTags.getCount()));
        //txtTotalCount.setText(String.format(Locale.US, "%d", m_totalCount));

        m_timeFlag = m_timeFlag xor 1
        if (0 == m_timeFlag) {
            //m_tagTpsCount = m_totalCount / m_timeSec
            m_timeSec++
        }

    }

}
