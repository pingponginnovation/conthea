package com.example.pingpongalien.conthea.ModelsRoom



/**
 * Created by AOR on 5/12/17.
 */
data class RoomCatalogProduct (
        var idProduct: Int?,
        var codigoProducto: String?,
        var fabricante: String?,
        var nombreProducto: String?
)