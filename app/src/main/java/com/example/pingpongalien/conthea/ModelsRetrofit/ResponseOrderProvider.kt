package com.example.pingpongalien.conthea.ModelsRetrofit


import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ResponseOrderProvider(

	@field:SerializedName("codigo")
	val codigo: String? = null,

	@field:SerializedName("estado")
	val estado: String? = null,

	@field:SerializedName("tipo")
	val tipo: String? = null,

	@field:SerializedName("categoria")
	val categoria: String? = null,

	@field:SerializedName("direccion")
	val direccion: String? = null,

	@field:SerializedName("ultimo_cambio")
	val ultimoCambio: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null,

	@field:SerializedName("rfc")
	val rfc: String? = null,

	@field:SerializedName("pais")
	val pais: String? = null,

	@field:SerializedName("beneficiario")
	val beneficiario: Any? = null,

	@field:SerializedName("estatus")
	val estatus: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("alta")
	val alta: Any? = null,

	@field:SerializedName("es_proveedor")
	val esProveedor: Int? = null,

	@field:SerializedName("rama")
	val rama: String? = null,

	@field:SerializedName("ciudad")
	val ciudad: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("es_cliente")
	val esCliente: Int? = null,

	@field:SerializedName("familia")
	val familia: String? = null
) : Serializable