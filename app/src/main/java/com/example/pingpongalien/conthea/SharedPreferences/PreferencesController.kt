package com.example.pingpongalien.conthea.SharedPreferences

import android.content.Context
import android.content.SharedPreferences
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLoginToken
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLoginUser

/**
 * Created by AOR on 22/11/17.
 */
class PreferencesController(context:Context) {

    val TAG = "PreferencesController"

    val NAME_PREFERENCES = "CONTHEA"
    val TOKEN = "TOKEN"
    val TOKEN_TYPE = "TOKEN_TYPE"
    val TOKEN_REFRESH = "TOKEN_REFRESH"
    val KEEP_SESSION = "KEEP_SESSION"
    val URL = "URL"
    val NAME = "NAME"
    val LAST_NAME = "LAST_NAME"
    val READ_TAGS = "READ_TAGS"
    val SAVED_TAGS = "SAVED_TAGS"

    companion object {
        var preferences: SharedPreferences? = null
    }

    init {
        if(preferences == null)
            preferences = context.getSharedPreferences(NAME_PREFERENCES, Context.MODE_PRIVATE)
    }

    fun saveToken(responseToken: ResponseLoginToken){
        val editor = preferences!!.edit()
        editor.putString(TOKEN_TYPE, responseToken.tokenType)
        editor.putString(TOKEN, responseToken.accessToken)
        editor.putString(TOKEN_REFRESH, responseToken.refreshToken)
        editor.apply()
    }

    fun saveUser(responseUser: ResponseLoginUser){
        val editor = preferences!!.edit()
        editor.putString(NAME, responseUser.name)
        editor.putString(LAST_NAME, responseUser.lastName)
        editor.apply()
    }

    fun saveSession(sessionState: Boolean){
        val editor = preferences!!.edit()
        editor.putBoolean(KEEP_SESSION, sessionState)
        editor.apply()
    }

    fun saveRead(read: String){
        val editor = preferences!!.edit()
        editor.putString(READ_TAGS, read)
        editor.apply()
    }

    fun saveSaved(saved: String){
        val editor = preferences!!.edit()
        editor.putString(SAVED_TAGS, saved)
        editor.apply()
    }


    fun closeSession(){
        val editor = preferences!!.edit()
        editor.putString(TOKEN, "")
        editor.apply()
    }

    fun getStateSession():Boolean {
        if (preferences!!.getString(TOKEN, "") != "")
            return preferences!!.getBoolean(KEEP_SESSION, false)
        return false
    }

    fun getUser(): ResponseLoginUser{
        return ResponseLoginUser(preferences!!.getString(LAST_NAME, ""), preferences!!.getString(NAME, ""))
    }

    fun saveUrl(url : String){
        val editor = preferences!!.edit()
        editor.putString(URL, url)
        editor.apply()
    }

    fun getUrl(): String{
        return preferences!!.getString(URL, "")
    }

    fun getToken() = preferences!!.getString(TOKEN, "")

    fun getRead() = preferences!!.getString(READ_TAGS, "0")

    fun getSaved() = preferences!!.getString(SAVED_TAGS, "0")
}