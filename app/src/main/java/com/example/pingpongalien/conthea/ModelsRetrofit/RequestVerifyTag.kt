package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class RequestVerifyTag(

	@field:SerializedName("lote")
	val lote: List<String?>? = null,

	@field:SerializedName("uuid")
	val uuid: List<String?>? = null
)