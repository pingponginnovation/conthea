package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseVerifyTag(

	@field:SerializedName("response")
	val response: List<ResponseVerifyTagItem?>? = null
)