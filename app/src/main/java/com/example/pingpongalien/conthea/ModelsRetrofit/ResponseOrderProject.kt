package com.example.pingpongalien.conthea.ModelsRetrofit


import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ResponseOrderProject(

	@field:SerializedName("estatus")
	val estatus: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null
) : Serializable