package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by ${AOR} on 27/11/17.
 */
data class ResponseOrderLote(

        @field:SerializedName("lote")
        val lote: String? = null,

        @field:SerializedName("caducidad")
        val caducidad: String? = null
) : Serializable