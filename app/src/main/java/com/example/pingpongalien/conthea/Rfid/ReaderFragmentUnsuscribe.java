package com.example.pingpongalien.conthea.Rfid;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;

import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;

import com.atid.lib.dev.ATRfidManager;
import com.atid.lib.dev.ATRfidReader;
import com.atid.lib.dev.event.RfidReaderEventListener;
import com.atid.lib.dev.rfid.type.ActionState;
import com.atid.lib.dev.rfid.type.ConnectionState;
import com.atid.lib.dev.rfid.type.ResultCode;
import com.atid.lib.diagnostics.ATLog;
import com.example.pingpongalien.conthea.R;

import static com.atid.lib.dev.ATScanManager.finish;

public abstract class ReaderFragmentUnsuscribe extends Fragment implements RfidReaderEventListener {

    // ------------------------------------------------------------------------
    // Member Variable
    // ------------------------------------------------------------------------

    private static final String TAG = ReaderActivity.class.getSimpleName();

    protected ATRfidReader mReader;
    protected int mView;


    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------

    public ReaderFragmentUnsuscribe() {
        super();
        mReader = null;
        mView = 0;
    }

    // ------------------------------------------------------------------------
    // Activit Event Handler
    // ------------------------------------------------------------------------


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unsuscribe, container, false);
        mView = view.getId();
        getActivity().setContentView(mView);
        // Initialize RFID Reader
        if ((mReader = ATRfidManager.getInstance()) == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setIcon(android.R.drawable.ic_dialog_alert);
            builder.setTitle(R.string.module_error);
            builder.setMessage(R.string.fail_check_module);
            builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }

            });
        }

        ATLog.i(TAG, "INFO. onCreate()");
        return view;
    }

    @Override
    public void onDestroy() {
        // Deinitalize RFID reader Instance
        ATRfidManager.onDestroy();
        ATLog.i(TAG, "INFO. onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        mReader = ATRfidManager.getInstance();
        if (mReader != null) {
            ATRfidManager.wakeUp();

            if (mReader.getState() == ConnectionState.Connected) {
                onInitReader(false);
            }
        }
        ATLog.i(TAG, "INFO. onStart()");
    }

    @Override
    public void onStop() {
        ATRfidManager.sleep();
        ATLog.i(TAG, "INFO. onStop()");
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mReader != null)
            mReader.setEventListener(this);

        ATLog.d(TAG, "INFO onResume()");
    }

    @Override
    public void onPause() {
        if (mReader != null)
            mReader.removeEventListener(this);

        ATLog.i(TAG, "INFO. onPause()");
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // ------------------------------------------------------------------------
    // Reader Event Handler
    // ------------------------------------------------------------------------

    @Override
    public void onReaderStateChanged(ATRfidReader reader, ConnectionState state) {
        switch (state) {
            case Connected:
                onInitReader(true);
                break;
        }
        ATLog.i(TAG, "EVENT. onReaderStateChanged(%s)", state);
    }

    @Override
    public void onReaderActionChanged(ATRfidReader reader, ActionState action) {
        ATLog.i(TAG, "EVENT. onReaderActionchanged(%s)", action);
    }

    @Override
    public void onReaderReadTag(ATRfidReader reader, String tag, float rssi, float phase) {
        ATLog.i(TAG, "EVENT. onReaderReadTag([%s], %.2f, %.2f)", tag, rssi, phase);
    }

    @Override
    public void onReaderResult(ATRfidReader reader, ResultCode code, ActionState action, String epc, String data,
                               float rssi, float phase) {
        ATLog.i(TAG, "EVENT. onReaderResult(%s, %s, [%s], [%s], %.2f, %.2f", code, action, epc, data, rssi, phase);
    }

    // ------------------------------------------------------------------------
    // Override Widgets Control Methods
    // ------------------------------------------------------------------------


    // Initialize Reader
    protected abstract void initReader();

    // Activated Reader
    protected abstract void activateReader();

    // Begin Initialize Reader
    private void onInitReader(final boolean enabled) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                initReader();
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        activateReader();
                    }

                });
            }

        }).start();
    }
}