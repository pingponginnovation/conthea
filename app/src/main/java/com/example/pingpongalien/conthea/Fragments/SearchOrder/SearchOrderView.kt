package com.example.pingpongalien.conthea.Fragments.SearchOrder


import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.pingpongalien.conthea.Activities.DetailOrder.DetailOrderView
import com.example.pingpongalien.conthea.Adapters.AdapterOrders
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.fragment_search_order.*
import org.jetbrains.anko.toast
import android.widget.AdapterView
import java.util.*

//typeSearch = true = OUT
class SearchOrderView(val typeSearch: Boolean) : Fragment(), SearchOrder.View {

    val TAG = "SearchOrderView"

    var presenter: SearchOrder.Presenter? = null

    var adapter: AdapterOrders? = null

    lateinit var datePickerStart: DatePickerDialog

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_search_order, container, false)
        presenter = SearchOrderPresenter(this, context)
        configureDatePickerDialog()
        presenter!!.setTypeSearch(typeSearch)
        return view
    }

    override fun onResume() {
        super.onResume()
        eventsView()
        getOrders()
        setOptionSearch()
    }

    override fun onPause() {
        super.onPause()
        rcv_orders.visibility = View.GONE
        pb_search_order.visibility = View.VISIBLE
    }

    override fun getOrders() {
        presenter!!.getOrders()
    }

    override fun setOrders(responseSuccess: ResponseOrder) {
        pb_search_order.visibility = View.GONE
        rcv_orders.layoutManager = LinearLayoutManager(context)
        adapter =  AdapterOrders(responseSuccess){
            showDetailOrder(it)
        }
        rcv_orders.adapter = adapter
    }

    override fun responseErrorOrders(error: String){
        context.toast(error)
    }

    override fun showNoOrders() {
        pb_search_order.visibility = View.GONE
        txt_no_orders.visibility = View.VISIBLE
        rcv_orders.visibility = View.GONE
    }

    override fun showOrders() {
        txt_no_orders.visibility = View.GONE
        rcv_orders.visibility = View.VISIBLE
    }
    //TRUE SHOW DATE - FALSE HIDE DATE
    override fun visibilityDateAndSearchBar(type : Boolean){
        if(type){
            edt_search.visibility = View.GONE
            txt_date.visibility = View.VISIBLE
        } else{
            edt_search.visibility = View.VISIBLE
            txt_date.visibility = View.GONE
        }

    }

    override fun setDate(date: String) {
        txt_date.text = date
    }

    fun eventsView(){
        edt_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                presenter!!.filterOrders(editable.toString(), adapter, sp_type_search.selectedItemPosition)
            }
        })
        sp_type_search.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                presenter!!.setComponentsCorrect(sp_type_search.selectedItemPosition)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {

            }

        })

        txt_date.setOnClickListener { datePickerStart.show() }
    }

    private fun showDetailOrder(order:ResponseOrderItem){
        val intent = Intent(context, DetailOrderView::class.java).putExtra("order", order).putExtra("type", typeSearch)
        context.startActivity(intent)
    }


    private fun setOptionSearch(){
        sp_type_search.adapter = ArrayAdapter<String>(context, R.layout.item_spinner, arrayListOf("Número de pedido", "Cliente", "Fecha"))
    }

    private fun configureDatePickerDialog(){
        val calendar = Calendar.getInstance()
        datePickerStart = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _ , year, monthOfYear, dayOfMonth ->
            presenter!!.formatDateAndFilterData(year, monthOfYear, dayOfMonth, adapter)
        },calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    }

}
