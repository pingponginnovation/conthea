package com.example.pingpongalien.conthea.ModelsRoom

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.example.pingpongalien.conthea.Helpers.HelperDate
import java.util.Date

/**
 * Created by AOR on 5/12/17.
 */
@Entity(tableName = "catalogo")
data class RoomCatalogItem(
        @PrimaryKey(autoGenerate = true)
        var productosId: Int?,
        @TypeConverters(HelperDate::class)
        var caducidad: Date?,
        @Embedded
        var espacioActual: RoomCatalogPlace?,
        var lote: String?,
        var cantidad: Int?,
        @Embedded
        var producto: RoomCatalogProduct?

)