package com.example.pingpongalien.conthea.ModelsRetrofit

data class ResponseGetPlacesStorage(
	val codigo: String? = null,
	val id: Int? = null,
	val nombre: String? = null
)
