package com.example.pingpongalien.conthea.Fragments.Unsuscribe

import android.content.Context
import android.view.MenuItem

class UnsuscribePresenter(viewUnsuscribe : Unsuscribe.View, context: Context): Unsuscribe.Presenter {

    var view: Unsuscribe.View? = null
    var model: Unsuscribe.Model? = null

    init {
        this.view = viewUnsuscribe
        model = UnsuscribeModel(this, context)
    }

    //MODEL
    override fun sendTags() {
        model!!.sendTags()
    }

    override fun addTag(tag: String) {
        model!!.addTag(tag)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model!!.setModeRead(nameItem, item)
    }

    //VIEW
    override fun setTotalUpdate(totalUpdate: String) {
        view!!.setTotalUpdate(totalUpdate)
    }

    override fun unSuccessRequestPlaces(error: String) {
        view!!.showErrorGetPlaces(error)
    }

    override fun setModeRead(mode: Boolean) {
        view!!.setModeRead(mode)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view!!.setItemNameMenu(name, item)
    }
}