package com.example.pingpongalien.conthea.Fragments.StartInventory

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestStartInventory
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseStartInventory
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

class StartInventoryModel(presenter : StartInventory.Presenter, context: Context, preferences: PreferencesController): StartInventory.Model {

    val presenter: StartInventory.Presenter = presenter
    val appContext: Context = context
    val preferences: PreferencesController = preferences
    var aTags = arrayListOf<String>()

    override fun addTag(tag: String) {
        aTags.add(tag)
    }

    override fun requestStartInventory() {
        val retrofit = RetrofitController(appContext)
        retrofit.sendStartInventory(RequestStartInventory(aTags), this)
    }

    override fun successStartInventory(responseSuccess: ResponseStartInventory?) {
        presenter.successStartInventory(responseSuccess)
        preferences.saveRead(responseSuccess?.response!![0].read!!)
        preferences.saveSaved(responseSuccess.response!![1].saved!!)
        aTags.clear()
    }

    override fun unSuccessStartInventory(error: String) {
        presenter.unSuccessStartInventory(error)
        aTags.clear()
    }

    override fun getData() {
        presenter.setData(preferences.getRead(), preferences.getSaved())
    }

    override fun resetData() {
        preferences.saveRead("0")
        preferences.saveSaved("0")
        presenter.setData(preferences.getRead(), preferences.getSaved())
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura continua")) {
            presenter.setModeRead(true)
            presenter.setItemNameMenu("Lectura única", item)
        } else{
            presenter.setModeRead(false)
            presenter.setItemNameMenu("Lectura continua", item)
        }
    }
}