package com.example.pingpongalien.conthea.Fragments.SetPlace

import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsePlaces

/**
 * Created by AOR on 5/12/17.
 */
interface SetPlace {

    interface View{
        fun setPlaces(aPlaces : ArrayList<String>)
        fun setNamePlace(name : String)
        fun showErrorGetPlaces(error : String)
        fun setTotalUpdate(totalUpdate : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
    }

    interface Presenter{
        //MODEL
        fun getPlaces()
        fun setidPlace(position : Int)
        fun getName(position : Int)
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)

        //VIEW
        fun setPlaces(aPlaces : ArrayList<String>)
        fun unSuccessRequestPlaces(error : String)
        fun setNamePlace(name : String)
        fun setTotalUpdate(totalUpdate : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
    }

    interface Model{
        fun requestPlaces()
        fun successRequestGetPlaces(response : ResponseGetPlaces)
        fun unSuccessRequestGetPlaces(error : String)
        fun successRequestPlaces(response : ResponsePlaces)
        fun setidPlace(position : Int)
        fun getName(position : Int)
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)
    }
}