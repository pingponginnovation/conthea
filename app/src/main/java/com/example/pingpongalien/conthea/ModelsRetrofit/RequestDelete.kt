package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName
import retrofit2.http.Field

data class RequestDelete(
    @field:SerializedName("tags")
    var tags: List<String>? = null
)
