package com.example.pingpongalien.conthea.Fragments.DetailTag

import android.content.Context
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDetailTagDetail

class DetailTagPresenter(viewDetailTag: DetailTag.View, context: Context): DetailTag.Presenter {

    var view: DetailTag.View? = null
    var model: DetailTag.Model? = null

    init {
        this.view = viewDetailTag
        this.model = DetailTagModel(this, context)
    }
    //MODEL
    override fun searchDetailTag(tag: String) {
        model!!.searchDetailTag(tag)
    }

    //VIEW
    override fun showErrorDetailTag(error: String) {
        view!!.showErrorDetailTag(error)
    }

    override fun setDetailTag(detailTag: ResponseDetailTagDetail) {
        view!!.setDetailTag(detailTag)
    }
}