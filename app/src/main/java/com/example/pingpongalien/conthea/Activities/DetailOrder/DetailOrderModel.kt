package com.example.pingpongalien.conthea.Activities.DetailOrder

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.Adapters.AdapterProducts
import com.example.pingpongalien.conthea.Models.Product
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

/**
 * Created by AOR on 28/11/17.
 */
class DetailOrderModel(presenter: DetailOrderPresenter, context : Context) : DetailOrder.Model {

    val TAG = "DetailOrderModel"

    var presenter: DetailOrder.Presenter? = null
    var appContext: Context? = null

    var aLotes = ArrayList<String>()
    var aProductLote = ArrayList<Product>()
    var aProducts: ArrayList<ResponseOrderProduct> = ArrayList()
    var adapterProducts: AdapterProducts? = null
    var aTags: ArrayList<RequestRemisionProducts> = ArrayList()
    var requestRemision = RequestRemision()
    var requestRemisionProducts = RequestRemisionProducts()
    var aTagsForVerify = arrayListOf<String>()
    var aTagsNoMatch = arrayListOf<String>()
    var shouldAddNoMatch = true
    var totalRead = 0
    private var typeSearch = false

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun successRequestVerify(responseVerifyTag: ResponseVerifyTag) {
        if(responseVerifyTag.response!!.isNotEmpty()) {
            responseVerifyTag.response.forEach { tag ->
                if (!wasReader(tag!!.uuid!!)) {
                    updateProducts(tag)
                }
            }
            filterTagsNoRepeat()
            filterTagsNoMatchFromRequest(responseVerifyTag)
            filterTagsNoRepeatNoMatch()
            if(aTagsNoMatch.size > 0)
                requestNameUuid(RequestNameUuid(aTagsNoMatch))
            cleanTagsForVerify()
        } else {
            filterTagsNoRepeat()
            requestNameUuid(RequestNameUuid(aTagsForVerify))
        }
        orderProducts()
        updateRecyclerView()
        presenter!!.setTotal(totalRead)
    }

    override fun successRequestNameUuid(response: ResponseNameUuid) {
        presenter!!.tagsNoMatch(createListNoMatchTotal(response.resultado!!))
        cleanTagsNoMatch()
    }

    override fun successRequestRemision(response: ResponseRemision) {
        presenter!!.successRequestRemision(response.response)
    }

    override fun unsuccessRequestRemision(error: String) {
        presenter!!.unsuccessRequestRemision(error)
    }

    override fun setLotes(aProducts: List<ResponseOrderProduct>) {
        aProducts.forEach { product ->
            product.tagsLoteSalida!!.forEach { lote ->
                aLotes.add(lote!!.lote!!)
                aProductLote.add(Product(product.id!!, lote.lote!!))
            }
        }
    }

    override fun setProducts(aProducts: List<ResponseOrderProduct>) {
        this.aProducts = aProducts as ArrayList<ResponseOrderProduct>
    }

    override fun addTagForVerify(tag: String) {
        aTagsForVerify.add(tag)
    }

    override fun searchTag() {
        if(aProductLote.size > 0)
           requestVerifyTags()
    }

    override fun setAdapter(adapter: AdapterProducts) {
        this.adapterProducts = adapter
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura continua")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura única", item)
        } else{
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura continua", item)
        }
    }

    override fun sendData(totalProducts : Int){
        if(aTags.size > 0) {
            if(totalProducts == aTags.size) {
                this.requestRemision.productos = aTags
                val retrofit = RetrofitController(this.appContext!!)
                retrofit.sendRemision(this.requestRemision, this, typeSearch)
            } else
                presenter!!.showNoCompleteOrder()
        } else
            presenter!!.showNoProductsToSend()
    }

    override fun sendData() {
        this.requestRemision.productos = aTags
        val retrofit = RetrofitController(this.appContext!!)
        retrofit.sendRemision(this.requestRemision, this, typeSearch)
    }

    override fun setTypeSearch(typeSearch: Boolean) {
        this.typeSearch = typeSearch
    }

    private fun filterTagsNoRepeat(){
        val aDataNoRepeat = HashSet<String>()
        aDataNoRepeat.addAll(aTagsForVerify)
        cleanTagsForVerify()
        aTagsForVerify.clear()
        aTagsForVerify.addAll(aDataNoRepeat)
    }

    private fun filterTagsNoRepeatNoMatch(){
        val aDataNoRepeat = HashSet<String>()
        aDataNoRepeat.addAll(aTagsNoMatch)
        cleanTagsForVerify()
        aTagsNoMatch.clear()
        aTagsNoMatch.addAll(aDataNoRepeat)
    }

    private fun requestVerifyTags(){
        val retrofit = RetrofitController(this.appContext!!)
        retrofit.verifyTags(RequestVerifyTag(aLotes, aTagsForVerify), this, typeSearch)
    }

    private fun requestNameUuid(requestNameUuid: RequestNameUuid){
        val retrofit = RetrofitController(this.appContext!!)
        retrofit.getNameUuid(requestNameUuid, this)
    }

    private fun updateProducts(responseVerifyTag: ResponseVerifyTagItem) {
        aProducts.forEachIndexed { index, product ->
            if (product.id!! == responseVerifyTag.id && aProductLote[index].lote == responseVerifyTag.lote) {
                if(product.cantidadRegistrada < product.cantidad!!) {
                    product.cantidadRegistrada++
                    addTagReaded(product.origenId!!, responseVerifyTag.uuid!!)
                    if (product.cantidadRegistrada == product.cantidad) {
                        product.validate = true
                        totalRead++
                    }
                }
            }
        }
    }

    private fun filterTagsNoMatchFromRequest(responseVerifyTag: ResponseVerifyTag){
        aTagsForVerify.forEach { tag ->
            responseVerifyTag.response!!.forEach { tagFromServer ->
                if(tag == tagFromServer!!.uuid)
                    this.shouldAddNoMatch = false
            }
            if(this.shouldAddNoMatch)
                aTagsNoMatch.add(tag)
            this.shouldAddNoMatch = true
        }

    }
    private fun orderProducts(){
        val aProductsDisorder = aProducts
        this.aProducts = ArrayList()
        var aProducts = ArrayList<ResponseOrderProduct>()
        aProductsDisorder.forEach { product ->
            if(!product.validate)
                aProducts.add(product)
        }
        aProductsDisorder.forEach{ product ->
            if(product.validate)
                aProducts.add(product)
        }
        this.aProducts = aProducts
    }

    private fun updateRecyclerView(){
        adapterProducts!!.swap(aProducts)
    }

    private fun cleanTagsForVerify(){
        aTagsForVerify.clear()
    }

    private fun cleanTagsNoMatch(){
        aTagsNoMatch.clear()
    }

    private fun wasReader(tag : String): Boolean{
        aTags.forEach{ tagInner ->
            tagInner.tags!!.forEach { product ->
                if(tag == product )
                    return true
            }
        }
        return false
    }

    private fun addTagReaded(originId: Int, tag: String) {
        aTags.forEach { tagInner ->
            if (originId == tagInner.origenId) {
                tagInner.tags!!.add(tag)
                return
            }
        }
        aTags.add(RequestRemisionProducts(originId, arrayListOf(tag)))
    }

    private fun createListNoMatchTotal(aTagsNoMatch: List<String>): ArrayList<TagNoMatch> {
        var shouldAdd: Boolean
        val aNoMatchTotal = ArrayList<TagNoMatch>()
        aTagsNoMatch.forEach { tag ->
            shouldAdd = true
            if (aNoMatchTotal.size > 0) {
                for(tagNoMatch in aNoMatchTotal){
                    if (tag == tagNoMatch.name) {
                        tagNoMatch.amount++
                        shouldAdd = false
                    }
                }
            }
            if(shouldAdd)
                aNoMatchTotal.add(TagNoMatch(tag, 1))

        }
        return aNoMatchTotal
    }

}