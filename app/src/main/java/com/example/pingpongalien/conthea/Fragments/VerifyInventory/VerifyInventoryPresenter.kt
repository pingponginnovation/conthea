package com.example.pingpongalien.conthea.Fragments.VerifyInventory

import android.content.Context
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem

/**
 * Created by AOR on 7/12/17.
 */
class VerifyInventoryPresenter(view: VerifyInventory.View, context: Context) : VerifyInventory.Presenter {

    var view: VerifyInventory.View? = null
    var model: VerifyInventory.Model? = null

    init {
        this.view = view
        this.model = VerifyInventoryModel(this, context)
    }

    //MODEL
    override fun getProducts() {
        model!!.getProducts()
    }

    override fun getPlaces(position: Int) {
        model!!.getPlaces(position)
    }

    override fun getOnePlace(position: Int) {
        model!!.getOnePlace(position)
    }


    //VIEW
    override fun setProducts(aProducts: ArrayList<String>) {
        view!!.setProducts(aProducts)
    }

    override fun unsuccessProducts(error: String) {
        view!!.unsuccessProducts(error)
    }

    override fun setPlaces(aPlaces: List<Map<String, String>>) {
        view!!.setPlaces(aPlaces)
    }

    override fun showDetailVerify(catalog: ResponseCatalogItem) {
        view!!.showDetailVerify(catalog)
    }

    override fun visibilityWithoutProducts() {
        view!!.visibilityWithoutProducts()
    }

}