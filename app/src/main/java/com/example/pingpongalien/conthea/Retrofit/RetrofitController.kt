package com.example.pingpongalien.conthea.Retrofit

import android.content.Context
import com.example.pingpongalien.conthea.Activities.DetailOrder.DetailOrder
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerify
import com.example.pingpongalien.conthea.Activities.Login.Login.Login
import com.example.pingpongalien.conthea.Fragments.DetailTag.DetailTag
import com.example.pingpongalien.conthea.Fragments.SearchCatalogue.SearchCatalogue
import com.example.pingpongalien.conthea.Fragments.SearchOrder.SearchOrder
import com.example.pingpongalien.conthea.Fragments.SetPlace.SetPlace
import com.example.pingpongalien.conthea.Fragments.StartInventory.StartInventory
import com.example.pingpongalien.conthea.Fragments.Unsuscribe.Unsuscribe
import com.example.pingpongalien.conthea.Fragments.VerifyInventory.VerifyInventory
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


class RetrofitController(context : Context) {

    val TAG = "RetrofitController"

    val URL_DEBUG get() = "https://conthea.com/api/"

    var URL = ""

    var api: Api? = null


    var preferences: PreferencesController

    init {
        URL = PreferencesController(context).getUrl()
        configurateRetrofit()
        preferences = PreferencesController(context)
    }

    fun configurateRetrofit(){
        val httpClient = configurateClient()
        val client = httpClient.build()
        val adapterRetrofit = Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        api = adapterRetrofit.create(Api::class.java)
    }

    private fun configurateClient(): OkHttpClient.Builder {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                val original = chain.request()
                val request = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build()
                return chain.proceed(request)
            }
        })
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        return httpClient
    }

    fun login(model: Login.Model, user: String, password: String, stateSession: Boolean){
        var responseSuccess: ResponseLogin? = null
        var responseError: ResponseError? = null
        doAsync {
            val callLogin  = api!!.login(RequestLogin(user, password))
            val response = callLogin.execute()
            if(response.isSuccessful) {
                responseSuccess = response.body()
            }else{
                responseError = ResponseError.fromResponseBody(response.errorBody()!!)
            }
            uiThread {
                if(responseSuccess != null)
                    model.successRequestLogin(responseSuccess!!, stateSession)
                else
                    model.errorRequestLogin(responseError!!)
            }
        }
    }

    fun getOrders(model: SearchOrder.Model, typeSearch:Boolean){
        var responseSuccess: ResponseOrder? = null
        doAsync {
            val callOrders = if (typeSearch) {
                api!!.getOrdersOut("Bearer ${preferences.getToken()}")
            } else {
                api!!.getOrdersIn("Bearer ${preferences.getToken()}")
            }
            val response = callOrders.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if( responseSuccess != null) {
                    model.responseOrderSuccess(responseSuccess!!)
                }else{
                    model.responseErrorOrders("No se obtuvieron las ordenes del servidor")
                }
            }
        }
    }

    fun verifyTags(requestVerifyTag: RequestVerifyTag, model : DetailOrder.Model, typeSearch:Boolean){
        var responseSuccess: ResponseVerifyTag? = null
        doAsync {
            val callTag = if (typeSearch) {
                api!!.verifyTagsOut("Bearer ${preferences.getToken()}", requestVerifyTag)
            } else {
                api!!.verifyTagsIn("Bearer ${preferences.getToken()}", requestVerifyTag)
            }
            val response = callTag.execute()
            if ( response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if( responseSuccess != null) {
                    model.successRequestVerify(responseSuccess!!)
                }
            }
        }
    }

    fun getNameUuid(requestNameUuid: RequestNameUuid, model : DetailOrder.Model){
        var responseSuccess: ResponseNameUuid? = null
        doAsync {
            val callTag = api!!.getNameUuid("Bearer ${preferences.getToken()}", requestNameUuid)
            val response = callTag.execute()
            if ( response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if( responseSuccess != null) {
                    model.successRequestNameUuid(responseSuccess!!)
                }
            }
        }
    }

    fun sendRemision(requestOrders: RequestRemision, model : DetailOrder.Model, typeSearch:Boolean){
        var responseSuccess: ResponseRemision? = null
        doAsync {
            val callRemision = if (typeSearch) {
                api!!.sendRemisionOut("Bearer ${preferences.getToken()}", requestOrders)
            } else {
                api!!.sendRemisionIn("Bearer ${preferences.getToken()}", requestOrders)
            }
            val response = callRemision.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successRequestRemision(responseSuccess!!)
                else {
                    if(response.code() == 422) {
                        model.unsuccessRequestRemision("Complete su pedido antes de darle salida a la orden")
                    } else {
                        model.unsuccessRequestRemision("Error en el servidor, intentelo de nuevo")
                    }
                }
            }
        }
    }

    fun sendStartInventory(requestStartInventory: RequestStartInventory, model : StartInventory.Model) {
        var responseSuccess: ResponseStartInventory? = null
        doAsync {
            val callStartInventory = api!!.startInventory("Bearer ${preferences.getToken()}", requestStartInventory)
            //Log.d(TAG + " Req", requestStartInventory.toString())
            val response = callStartInventory.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null) {
                    //Log.d(TAG + "Res", responseSuccess.toString())
                    model.successStartInventory(responseSuccess!!)
                }
                else {
                    model.unSuccessStartInventory("Sucedio un error al enviar los datos")
                }
            }
        }
    }

    fun getPlaces(model : SetPlace.Model){
        var responseSuccess: ResponseGetPlaces? = null
        doAsync {
            val callPlaces = api!!.getPlaces("Bearer ${preferences.getToken()}")
            val response = callPlaces.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successRequestGetPlaces(responseSuccess!!)
                else
                    model.unSuccessRequestGetPlaces("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun sendTags(requestPlaces : RequestPlaces, model : SetPlace.Model){
        var responseSuccess: ResponsePlaces? = null
        doAsync {
            val callPlaces = api!!.sendTags("Bearer ${preferences.getToken()}", requestPlaces)
            val response = callPlaces.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successRequestPlaces(responseSuccess!!)
                else
                    model.unSuccessRequestGetPlaces("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun getCataloge(model : SearchCatalogue.Model){
        var responseSuccess: ResponseCatalog? = null
        doAsync {
            val callCataloge = api!!.getCataloge("Bearer ${preferences.getToken()}")
            val response = callCataloge.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successGetCataloge(responseSuccess!!)
                else
                    model.unsuccessGetCataloge("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun getProducts(model : VerifyInventory.Model){
        var responseSuccess: List<ResponseProducts>? = null
        doAsync {
            val callProducts = api!!.getProducts("Bearer ${preferences.getToken()}")
            val response = callProducts.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successProducts(responseSuccess!!)
                else
                    model.unsuccessProducts("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun getPlaces(idProduct : Int, model : VerifyInventory.Model){
        var responseSuccess: ResponseCatalog? = null
        doAsync {
            val callPlaces = api!!.getPlaces("Bearer ${preferences.getToken()}", idProduct)
            val response = callPlaces.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successPlaces(responseSuccess!!)
                else
                    model.unsuccessProducts("Error en el servidor al obtener los lugares, intentelo de nuevo")
            }
        }
    }

    fun verifyUbication(verify:  RequestVerify, model : DetailVerify.Model){
        var responseSuccess: ResponseVerify? = null
        doAsync {
            val callVerify = api!!.verifyUbication("Bearer ${preferences.getToken()}", verify)
            val response = callVerify.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successVerify(responseSuccess!!)
                else
                    model.unsuccessVerify("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun deleteTags(requestDelete : RequestDelete, model : Unsuscribe.Model){
        var responseSuccess: ResponseDelete? = null
        doAsync {
            val callDelete = api!!.deleteProduct("Bearer ${preferences.getToken()}", requestDelete)
            val response = callDelete.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successRequestPlaces(responseSuccess!!)
                else
                    model.unSuccessRequestGetPlaces("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun detailTag(requestDetailTag: RequestDetailTag, model : DetailTag.Model){
        var responseSuccess: ResponseDetailTag? = null
        doAsync {
            val callDetail = api!!.detailTag("Bearer ${preferences.getToken()}", requestDetailTag)
            val response = callDetail.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successRequestDetailTag(responseSuccess!!)
                else
                    model.unsuccessRequestDetailTag("El item no esta registrado en el sistema")
            }
        }
    }

    fun detailTag(requestDetailTag: RequestDetailTag, model : SearchCatalogue.Model){
        var responseSuccess: ResponseDetailTag? = null
        doAsync {
            val callDetail = api!!.detailTag("Bearer ${preferences.getToken()}", requestDetailTag)
            val response = callDetail.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successDetailTag(responseSuccess!!)
                else
                    model.unsuccessDetailTag("El item no esta registrado en el sistema")
            }
        }
    }

}