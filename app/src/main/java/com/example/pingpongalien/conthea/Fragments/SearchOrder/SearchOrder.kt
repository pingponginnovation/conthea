package com.example.pingpongalien.conthea.Fragments.SearchOrder

import com.example.pingpongalien.conthea.Adapters.AdapterOrders
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder

/**
 * Created by AOR on 27/11/17.
 */
interface SearchOrder {

    interface View{
        fun getOrders()
        fun setOrders(responseSuccess: ResponseOrder)
        fun responseErrorOrders(error: String)
        fun showNoOrders()
        fun showOrders()
        fun visibilityDateAndSearchBar(type : Boolean)
        fun setDate(date : String)
    }

    interface Presenter{
        fun getOrders()
        fun responseOrderSuccess(responseSuccess: ResponseOrder)
        fun responseErrorOrders(error: String)
        fun showNoOrders()
        fun filterOrders(filter: String, adapterOrders: AdapterOrders?, typeSearch: Int)
        fun showOrders()
        fun setComponentsCorrect(position : Int)
        fun visibilityDateAndSearchBar(type : Boolean)
        fun formatDateAndFilterData(year: Int, month: Int, day: Int,  adapterOrders: AdapterOrders?)
        fun setDate(date : String)
        fun setTypeSearch(typeSearch: Boolean)
    }

    interface Model{
        fun getOrders()
        fun responseOrderSuccess(responseSuccess: ResponseOrder)
        fun responseErrorOrders(error: String)
        fun filterOrders(filter: String, adapterOrders: AdapterOrders?, typeSearch: Int)
        fun setComponentsCorrect(position : Int)
        fun formatDateAndFilterData(year: Int, month: Int, day: Int,  adapterOrders: AdapterOrders?)
        fun setTypeSearch(typeSearch: Boolean)
    }
}