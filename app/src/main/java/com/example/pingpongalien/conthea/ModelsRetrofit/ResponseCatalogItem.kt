package com.example.pingpongalien.conthea.ModelsRetrofit

import java.io.Serializable

data class ResponseCatalogItem(
		val caducidad: String? = null,
		val espacio_actual: ResponseCatalogPlace? = null,
		val lote: String? = null,
		val cantidad: Int? = null,
		val producto: ResponseCatalogProduct? = null,
		val productos_id: Int? = null
) : Serializable
