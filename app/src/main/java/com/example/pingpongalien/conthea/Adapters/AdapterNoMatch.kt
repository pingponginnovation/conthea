package com.example.pingpongalien.conthea.Adapters

import android.nfc.Tag
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.dialog_no_match.view.*
import kotlinx.android.synthetic.main.item_no_match.view.*

class AdapterNoMatch(var aTagNoMatch : ArrayList<TagNoMatch>)  : RecyclerView.Adapter<AdapterNoMatch.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_no_match, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(aTagNoMatch[position])

    override fun getItemCount(): Int = aTagNoMatch.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tags: StringBuilder = StringBuilder()
        fun bind(tagNoMatch: TagNoMatch) = with(itemView) {
            tags.setLength(0)
            this.txt_name_no_match.text = tagNoMatch.name
            this.txt_number_no_match.text = tagNoMatch.amount.toString()
        }
    }
}