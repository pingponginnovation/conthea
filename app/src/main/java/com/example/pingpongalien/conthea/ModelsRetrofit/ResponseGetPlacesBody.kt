package com.example.pingpongalien.conthea.ModelsRetrofit

data class ResponseGetPlacesBody(
        val codigo: String? = null,
        val updatedAt: Any? = null,
        val almacen: ResponseGetPlacesStorage? = null,
        val createdAt: Any? = null,
        val id: Int? = null,
        val almacenesId: Int? = null
)
