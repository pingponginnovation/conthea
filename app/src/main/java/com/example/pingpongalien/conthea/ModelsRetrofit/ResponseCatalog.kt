package com.example.pingpongalien.conthea.ModelsRetrofit

data class ResponseCatalog(
	val response: List<ResponseCatalogItem?>? = null
)
