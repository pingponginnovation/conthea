package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class RequestRemisionProducts (
    @SerializedName("origen_id")
    val origenId: Int? = null,
    @SerializedName("tags")
    var tags: ArrayList<String>? = null
)
