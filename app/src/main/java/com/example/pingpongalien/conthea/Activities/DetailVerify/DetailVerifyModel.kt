package com.example.pingpongalien.conthea.Activities.DetailVerify

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestVerify
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseVerify
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

/**
 * Created by AOR on 7/12/17.
 */
class DetailVerifyModel(presenter : DetailVerify.Presenter, context: Context) : DetailVerify.Model {

    var presenter: DetailVerify.Presenter? = null

    var aTagsForVerify = arrayListOf<String>()

    var product: ResponseCatalogItem? = null

    var appContext: Context

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun searchTag() {
        requestVerifyTags()
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura continua")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura única", item)
        } else{
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura continua", item)
        }
    }

    override fun addTagForVerify(tag: String) {
        aTagsForVerify.add(tag)
    }

    override fun successVerify(response: ResponseVerify) {
        setColor(response.response, product!!.cantidad!!)
        presenter!!.setAmount(response.response, product!!.cantidad!!)
        presenter!!.setTotal(response.response.toString())
        cleanTagsForVerify()
    }

    override fun unsuccessVerify(error: String) {
        presenter!!.unsuccessVerify(error)
    }

    override fun setCatalog(product: ResponseCatalogItem) {
        this.product = product
        presenter!!.setAmount(0, product.cantidad!!)
    }

    fun requestVerifyTags(){
        val retrofit = RetrofitController(appContext)
        retrofit.verifyUbication( createVerify(), this)
    }

    fun createVerify() : RequestVerify{
        return RequestVerify(product!!.espacio_actual!!.id, aTagsForVerify, product!!.producto!!.id, product!!.lote)
    }

    fun cleanTagsForVerify(){
        aTagsForVerify.clear()
    }

    fun setColor(amount : Int, total : Int){
        if (amount == total)
            presenter!!.setColorComplete()
        else
            presenter!!.setColorIncomplete()
    }

}