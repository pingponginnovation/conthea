package com.example.pingpongalien.conthea.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.item_order.view.*

/**
 * Created by AOR on 27/11/17.
 */
class AdapterOrders(var responseSuccess: ResponseOrder, val listener: (ResponseOrderItem) -> Unit) : RecyclerView.Adapter<AdapterOrders.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterOrders.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_order, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: AdapterOrders.ViewHolder, position: Int)  = holder.bind(responseSuccess.response!![position]!!, listener)

    override fun getItemCount(): Int  = responseSuccess.response!!.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(responseOrderItem: ResponseOrderItem, listener: (ResponseOrderItem) -> Unit) = with(itemView) {
            this.txt_order.text = responseOrderItem.codigo
            this.txt_date.text = responseOrderItem.fecha
            this.txt_customer.text = responseOrderItem.cliente!!.nombre
            setOnClickListener{listener(responseOrderItem)}
        }
    }

    fun filterList(orders: ResponseOrder) {
        this.responseSuccess = orders
        notifyDataSetChanged()
    }
}