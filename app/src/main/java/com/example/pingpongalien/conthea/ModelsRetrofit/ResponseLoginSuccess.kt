package com.example.pingpongalien.conthea.ModelsRetrofit

/**
 * Created by AOR on 22/11/17.
 */
data class ResponseLoginSuccess(val token_type: String,
                                val expires_in: String,
                                val access_token: String,
                                val refresh_token: String)