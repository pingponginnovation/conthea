package com.example.pingpongalien.conthea.Activities.Core

import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder


/**
 * Created by AOR on 23/11/17.
 */
interface Core {

    interface View{
        fun closeSession()
        fun showLoginView()
        fun setNameUser(name: String, lastName: String)
    }

    interface Presenter{
        fun closeSession()
        fun showLoginView()
        fun setNameUser(name: String, lastName: String)
        fun getUser()
    }

    interface Model{
        fun closeSession()
        fun getUser()
    }


}