package com.example.pingpongalien.conthea.ModelsRoom

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import java.util.*

/**
 * Created by AOR on 5/12/17.
 */
@Dao
interface DaoCatalog {

    @Insert
    fun insertCatalog(catalogItem: RoomCatalogItem)

    @Query("DELETE FROM catalogo")
    fun deleteCatalog()

    @Query("SELECT * FROM catalogo")
    fun getCataloges(): List<RoomCatalogItem>

    @Query("SELECT DISTINCT nombreProducto FROM catalogo" +
            " WHERE idProduct LIKE :id")
    fun getProductsById(id: String) : List<String>

    @Query("SELECT DISTINCT nombreProducto FROM catalogo" +
            " WHERE nombreProducto LIKE :name")
    fun getProductsByName(name: String) : List<String>

    @Query("SELECT * FROM catalogo" +
            " WHERE nombreProducto LIKE :name")
    fun getCatalogesByName(name: String): List<RoomCatalogItem>

    @Query("SELECT DISTINCT nombreProducto FROM catalogo" +
            " WHERE fabricante LIKE :maker")
    fun getProductsByMaker(maker: String) : List<String>

    @Query("SELECT * FROM catalogo" +
            " WHERE fabricante LIKE :maker")
    fun getCatalogesByMaker(maker: String): List<RoomCatalogItem>

    @Query("SELECT DISTINCT nombreProducto FROM catalogo" +
            " WHERE lote LIKE :lote")
    fun getProductsByLote(lote: String) : List<String>

    @Query("SELECT * FROM catalogo" +
            " WHERE lote LIKE :lote")
    fun getCatalogesByLote(lote: String): List<RoomCatalogItem>

    @Query("SELECT DISTINCT nombreProducto FROM catalogo" +
            " WHERE caducidad BETWEEN :startDate AND :finishDate")
    fun getProductsByExpiration(startDate : Date, finishDate : Date) : List<String>

    @Query("SELECT * FROM catalogo" +
            " WHERE caducidad LIKE :expiration")
    fun getCatalogesByExpiration(expiration: String): List<RoomCatalogItem>

}