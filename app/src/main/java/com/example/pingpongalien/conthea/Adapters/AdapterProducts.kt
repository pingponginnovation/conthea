package com.example.pingpongalien.conthea.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderProduct
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.item_products.view.*



/**
 * Created by AOR on 28/11/17.
 */
class AdapterProducts(var responseOrderProduct: ArrayList<ResponseOrderProduct>) : RecyclerView.Adapter<AdapterProducts.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_products, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(responseOrderProduct[position])

    override fun getItemCount(): Int = responseOrderProduct.size



    fun swap(aResponseOrderProduct: ArrayList<ResponseOrderProduct>?) {
        if (aResponseOrderProduct == null || aResponseOrderProduct.size == 0)
            return
        if (responseOrderProduct.size > 0)
            responseOrderProduct.clear()
        responseOrderProduct.addAll(aResponseOrderProduct)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tags: StringBuilder = StringBuilder()
        fun bind(responseOrderProduct: ResponseOrderProduct) = with(itemView) {
            tags.setLength(0)
            this.txt_product.text = responseOrderProduct.nombre
            this.txt_amount.text = "${responseOrderProduct.cantidadRegistrada} / ${responseOrderProduct.cantidad}"
            this.txt_customer.text = responseOrderProduct.fabricante
            responseOrderProduct.tagsLoteSalida!!.forEach { tag ->
                tags.append("${tag!!.lote} , ")
            }
            if(tags.isEmpty())
                tags.append("Sin lotes disponibles")
            this.txt_lote.text = tags
            if(responseOrderProduct.validate)
                this.im_state.setImageResource(R.drawable.ic_validate)
            else
                this.im_state.setImageResource(R.drawable.ic_invalidate)
        }

    }
}