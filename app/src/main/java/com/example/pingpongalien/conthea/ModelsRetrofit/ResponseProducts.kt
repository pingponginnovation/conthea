package com.example.pingpongalien.conthea.ModelsRetrofit

data class ResponseProducts(
	val id: Int? = null,
	val nombre: String? = null
)
