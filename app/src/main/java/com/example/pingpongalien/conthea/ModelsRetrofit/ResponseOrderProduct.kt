package com.example.pingpongalien.conthea.ModelsRetrofit


import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ResponseOrderProduct(

	@field:SerializedName("codigo")
	val codigo: String? = null,

	@field:SerializedName("origen_id")
	val origenId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("cantidad")
	val cantidad: Int? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null,

	@field:SerializedName("fabricante")
	val fabricante: String? = null,

	@field:SerializedName("tags_lote_salida")
	val tagsLoteSalida: List<ResponseOrderLote?>? = null,

	var validate : Boolean = false,

	var cantidadRegistrada: Int = 0
) : Serializable