package com.example.pingpongalien.conthea.Fragments.VerifyInventory

import android.content.Context
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalog
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseProducts
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread



/**
 * Created by AOR on 7/12/17.
 */
class VerifyInventoryModel(presenter: VerifyInventoryPresenter, context: Context) : VerifyInventory.Model {


    var presenter: VerifyInventory.Presenter? = null
    var aProducts: List<ResponseProducts> = ArrayList()
    var aPlaces: ResponseCatalog? = null
    var appContext: Context

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun getProducts() {
        requestProducts()
    }

    override fun successProducts(response: List<ResponseProducts>) {
        this.aProducts = response
        if(this.aProducts.size > 0)
            createListProducts()
        else
            presenter!!.visibilityWithoutProducts()
    }

    override fun unsuccessProducts(error: String) {
        presenter!!.unsuccessProducts(error)
    }

    override fun successPlaces(response: ResponseCatalog) {
        aPlaces = response
        createListPlaces()
    }

    override fun getPlaces(position: Int) {
        requestPlaces(aProducts[position].id!!)
    }

    override fun getOnePlace(position: Int) {
        presenter!!.showDetailVerify(aPlaces!!.response!!.get(position)!!)
    }

    fun requestProducts(){
        val retrofit = RetrofitController(appContext)
        retrofit.getProducts(this)
    }

    fun requestPlaces(idProduct : Int){
        val retrofit = RetrofitController(appContext)
        retrofit.getPlaces(idProduct,this)
    }

    fun createListProducts(){
        doAsync {
            val aInnerProducts = arrayListOf<String>()
            aProducts.forEach { product ->
                aInnerProducts.add(product.nombre!!)
            }
            uiThread {
                presenter!!.setProducts(aInnerProducts)
            }
        }
    }

    fun createListPlaces(){
        doAsync {
            val aInnerPlaces = ArrayList<Map<String, String>>()
            aPlaces!!.response!!.forEach { place ->
                val innerPlace = HashMap<String, String>(2)
                if(place!!.espacio_actual != null)
                    innerPlace.put("place", place.espacio_actual!!.codigo!!)
                else
                    innerPlace.put("place","Sin lugar asignado")
                if(place.lote != null)
                    innerPlace.put("lote", place.lote)
                else
                    innerPlace.put("lote","Sin lote asignado")
                aInnerPlaces.add(innerPlace)
            }
            uiThread {
                presenter!!.setPlaces(aInnerPlaces)
            }
        }
    }
}