package com.example.pingpongalien.conthea.ModelsRetrofit


import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ResponseOrderItem(

		@field:SerializedName("fecha")
	val fecha: String? = null,

		@field:SerializedName("origen_id")
	val origenId: Int? = null,

		@field:SerializedName("codigo")
		val codigo: String? = null,

		@field:SerializedName("almacen")
	val almacen: ResponseOrderStore? = null,

		@field:SerializedName("proyecto")
	val proyecto: ResponseOrderProject? = null,

		@field:SerializedName("cliente")
	val cliente: ResponseOrderProvider? = null,

		@field:SerializedName("productos")
	val productos: List<ResponseOrderProduct?>? = null
) : Serializable