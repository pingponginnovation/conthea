package com.example.pingpongalien.conthea.ModelsRetrofit

import java.io.Serializable

data class ResponseCatalogPlace(
	val codigo: String? = null,
	val id: Int? = null
) : Serializable
