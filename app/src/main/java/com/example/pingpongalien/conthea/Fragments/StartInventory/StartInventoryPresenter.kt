package com.example.pingpongalien.conthea.Fragments.StartInventory

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestStartInventory
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseStartInventory
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

class StartInventoryPresenter(viewUnsuscribe : StartInventory.View, context: Context, preferences: PreferencesController): StartInventory.Presenter {

    var view: StartInventory.View = viewUnsuscribe
    var model: StartInventory.Model = StartInventoryModel(this, context, preferences)

    //MODEL
    override fun requestStartInventory() {
        model.requestStartInventory()
    }

    override fun resetData() {
        model.resetData()
    }

    override fun getData() {
        model.getData()
    }

    override fun addTag(tag: String) {
        model.addTag(tag)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model.setModeRead(nameItem, item)
    }

    //View
    override fun unSuccessStartInventory(error: String) {
        view.unSuccessStartInventory(error)
    }

    override fun successStartInventory(responseSuccess: ResponseStartInventory?) {
        view.successStartInventory(responseSuccess)
    }

    override fun setData(read: String, saved: String) {
        view.setData(read, saved)
    }

    override fun setModeRead(mode: Boolean) {
        view.setModeRead(mode)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view.setItemNameMenu(name, item)
    }
}