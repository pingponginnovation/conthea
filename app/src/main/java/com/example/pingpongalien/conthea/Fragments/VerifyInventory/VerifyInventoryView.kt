package com.example.pingpongalien.conthea.Fragments.VerifyInventory


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter

import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.fragment_verify_inventory.*
import org.jetbrains.anko.toast
import android.content.Intent
import android.widget.SimpleAdapter
import android.widget.AdapterView.OnItemClickListener
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem


class VerifyInventoryView : Fragment(), VerifyInventory.View {


    var presenter: VerifyInventory.Presenter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_verify_inventory, container, false)
        presenter = VerifyInventoryPresenter(this, context)
        presenter!!.getProducts()
        return view
    }

    override fun onResume() {
        super.onResume()
        eventsView()
    }

    override fun setProducts(aProducts: List<String>) {
        sp_type_search.adapter = ArrayAdapter<String>(context, R.layout.item_spinner, aProducts)
    }

    override fun unsuccessProducts(error: String) {
        visibilityProgressBar(false)
        activity.toast(error)
    }

    override fun setPlaces(aPlaces : List<Map<String, String>>) {
        visibilityListView(true)
        visibilityProgressBar(false)
        lv_places.adapter = SimpleAdapter(context, aPlaces, android.R.layout.simple_list_item_2, arrayOf("place", "lote"), intArrayOf(android.R.id.text1, android.R.id.text2));
    }

    override fun showDetailVerify(catalog: ResponseCatalogItem) {
        val intent = Intent(getContext(), DetailVerifyView::class.java)
                .putExtra("catalogue", catalog)
                .putExtra("nameProduct", sp_type_search.selectedItem.toString())
        getContext().startActivity(intent)
    }

    override fun visibilityWithoutProducts(){
        im_arrow.visibility = View.GONE
        pb_search_order.visibility = View.GONE
        sp_type_search.visibility = View.GONE
        txt_message.visibility = View.VISIBLE
    }

    fun visibilityProgressBar(visibility : Boolean){
        if(visibility)
            pb_search_order.visibility = View.VISIBLE
        else
            pb_search_order.visibility = View.GONE
    }

    fun visibilityListView(visibility : Boolean){
        if(visibility)
            lv_places.visibility = View.VISIBLE
        else
            lv_places.visibility = View.GONE
    }

    fun eventsView() {
        sp_type_search.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                visibilityListView(false)
                visibilityProgressBar(true)
                presenter!!.getPlaces(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {

            }

        })
        lv_places.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                presenter!!.getOnePlace(position)
            }
        })
    }

}
