package com.example.pingpongalien.conthea.Room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.pingpongalien.conthea.ModelsRoom.RoomCatalogItem
import android.arch.persistence.room.Room
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.example.pingpongalien.conthea.Helpers.HelperDate
import com.example.pingpongalien.conthea.ModelsRoom.DaoCatalog


/**
 * Created by AOR on 5/12/17.
 */
@Database(entities = arrayOf(
        RoomCatalogItem::class
), version = 1, exportSchema = false)
@TypeConverters(HelperDate::class)
abstract class RoomController: RoomDatabase() {


    abstract fun daoController(): DaoCatalog

    companion object {


        private var INSTANCE: RoomController? = null

        fun getAppDatabase(context: Context): RoomController {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, RoomController::class.java, "conthea").fallbackToDestructiveMigration().build()
            }
            return INSTANCE!!
        }

        fun getInstance(): RoomController{
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}