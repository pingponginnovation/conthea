package com.example.pingpongalien.conthea.Fragments.SetPlace

import android.content.Context
import android.view.MenuItem

/**
 * Created by AOR on 5/12/17.
 */
class SetPlacePresenter(viewPlace : SetPlace.View, context: Context) : SetPlace.Presenter {

    var view: SetPlace.View? = null
    var model: SetPlace.Model? = null

    init {
        this.view = viewPlace
        this.model = SetPlaceModel(this, context)
    }

    //MODEL
    override fun getPlaces() {
        model!!.requestPlaces()
    }

    override fun setidPlace(position: Int) {
        model!!.setidPlace(position)
    }

    override fun getName(position: Int) {
        model!!.getName(position)
    }

    override fun sendTags() {
        model!!.sendTags()
    }

    override fun addTag(tag: String) {
        model!!.addTag(tag)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model!!.setModeRead(nameItem, item)
    }

    //VIEW
    override fun setPlaces(aPlaces: ArrayList<String>) {
        view!!.setPlaces(aPlaces)
    }

    override fun unSuccessRequestPlaces(error: String) {
        view!!.showErrorGetPlaces(error)
    }

    override fun setNamePlace(name: String) {
        view!!.setNamePlace(name)
    }

    override fun setTotalUpdate(totalUpdate: String) {
        view!!.setTotalUpdate(totalUpdate)
    }

    override fun setModeRead(mode: Boolean) {
        view!!.setModeRead(mode)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view!!.setItemNameMenu(name, item)
    }
}