package com.example.pingpongalien.conthea.Fragments.DetailTag

import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDetailTag
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDetailTagDetail

interface DetailTag {

    interface View{
        fun showErrorDetailTag(error : String)
        fun setDetailTag(detailTag : ResponseDetailTagDetail)

    }

    interface Presenter{
        //MODEL
        fun searchDetailTag(tag : String)


        //VIEW
        fun showErrorDetailTag(error : String)
        fun setDetailTag(detailTag : ResponseDetailTagDetail)

    }

    interface Model{
        fun searchDetailTag(tag : String)
        fun successRequestDetailTag(detailTag: ResponseDetailTag)
        fun unsuccessRequestDetailTag(error :String)
    }
}