package com.example.pingpongalien.conthea.Activities.DetailOrder

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.Adapters.AdapterProducts
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderProduct

/**
 * Created by AOR on 28/11/17.
 */
class DetailOrderPresenter(viewDetail: DetailOrder.View, context : Context) : DetailOrder.Presenter {

    var view: DetailOrder.View? = null
    var model: DetailOrder.Model? = null

    init {
        this.view = viewDetail
        model = DetailOrderModel(this, context)
    }

    override fun successRequestRemision(response: String) {
        view!!.successRequestRemision(response)
    }

    override fun unsuccessRequestRemision(error: String) {
        view!!.unsuccessRequestRemision(error)
    }

    override fun addTag(tag: String) {
        model!!.addTagForVerify(tag)
    }

    override fun setLotes(aProducts: List<ResponseOrderProduct>) {
        model!!.setLotes(aProducts)
    }

    override fun setProducts(aProducts: List<ResponseOrderProduct>) {
        model!!.setProducts(aProducts)
    }

    override fun setAdapter(adapter: AdapterProducts) {
        model!!.setAdapter(adapter)
    }

    override fun sendData(totalProducts : Int) {
        model!!.sendData(totalProducts)
    }

    override fun sendData() {
        model!!.sendData()
    }

    override fun setModeRead(mode: Boolean) {
        view!!.setModeRead(mode)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model!!.setModeRead(nameItem, item)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view!!.setItemNameMenu(name, item)
    }

    override fun searchTag() {
        model!!.searchTag()
    }

    override fun tagsNoMatch(aTagsNoMatch:  ArrayList<TagNoMatch>) {
        view!!.tagsNoMatch(aTagsNoMatch)
    }

    override fun setTotal(total: Int) {
        view!!.setTotal(total)
    }

    override fun showNoProductsToSend() {
        view!!.showNoProductsToSend()
    }

    override fun showNoCompleteOrder() {
        view!!.showNoCompleteOrder()
    }

    override fun setTypeSearch(typeSearch: Boolean) {
        model!!.setTypeSearch(typeSearch)
    }
}