package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseStartInventory (
    @SerializedName("response")
    var response : List<ResponseStartInventoryRead>? = null
)