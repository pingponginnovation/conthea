package com.example.pingpongalien.conthea.ModelsRetrofit

data class ResponsePlaces(
	val response: Boolean? = null,
	val elementos_actualizados: Int? = null
)
